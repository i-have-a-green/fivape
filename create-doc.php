<?php
/*
Template Name: CreateDoc
*/
if(isset($_POST['createFormDoc'])){
	$post = [];
	$post['post_type']   = 'doc';
	$post['post_status'] = 'draft';
	$post['post_title'] = sanitize_text_field($_POST['titre']);
	$post['post_content'] = sanitize_text_field($_POST['post_content']);

	$id = wp_insert_post( $post, true );
	wp_set_post_terms( $id, array($_POST['categorie']), "categorie");

	$image = rp_import_image('file',$id);
	if($image !== false){
		update_field( 'fichier', $image, $id );
	}

	wp_redirect(home_url( 'extranet/doc' ), 302);
	exit();
}


get_header('extranet'); ?>
<div id="pre-content">
	<div class="fil_ariane doc">
		<p>
			<?php the_title();?>
		</p>
	</div>
	<main id="content">
		<form method="post" action="" id="formCreateDoc" enctype="multipart/form-data">
			<div>
				<b>Titre du sujet</b><br />
				<input type="text" placeholder="Titre du document" name="titre" required />
			</div>
			<div>
				<b>Commentaires</b><br />
				<textarea placeholder="Création de post" name="post_content" /></textarea>

			</div>
			<div>
				<b>Categorie</b>
				<select name="categorie">
					<?php
					$terms = get_terms('categorie', array('hide_empty' => true));
					foreach($terms as $term){
						echo '<option value="'.$term->term_id.'">
						'.$term->name.'
						</option>';
					}
					?>
				</select>
			</div>
			<div>
				<input type="file" name="file" />
			</div>
			<div>
				<button type="submit" name="createFormDoc" class="button button-doc">Création du document</button>
			</div>
		</form>
	</main>
</div>
<?php get_footer('extranet'); ?>
