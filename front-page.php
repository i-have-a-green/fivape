<?php
get_header();
?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div id="pre-content">
			<?php get_template_part( 'parts/nbr', 'adherents' ); ?>
			<div id="presentation">
				<div id="presentation-item">
					<?php if( have_rows('blocs') ): ?>
					   <?php while ( have_rows('blocs') ) : the_row(); ?>
						   <div class="bloc">
							 <a href="<?php the_sub_field('lien');?>">
							   	 <?php echo wp_get_attachment_image( get_sub_field('icone'), 'full' );?>
							   	 <h2>
							   		 <?php the_sub_field('titre');?>
							   	 </h2>
							</a>
						   	 <p class="description">
						   		 <?php the_sub_field('description');?>
						   	 </p>
						   </div>
					   <?php endwhile;?>
				   <?php endif;?>
				</div>
				<div id="presenation-contenu">
					<?php the_field('contenu');?>
				</div>
			</div>
			<div id="actualite">
				<?php
				$args = array(	'post_type' => 'post',
								'post_status'    => 'publish',
								'posts_per_page' => 9);
				$myposts = get_posts( $args );
				$count = count($myposts);
				foreach ( $myposts as $post ) :
				  	setup_postdata( $post ); ?>
					<?php get_template_part( 'parts/loop', 'archive' ); ?>
				<?php endforeach;
				wp_reset_postdata();
				?>
			</div>

				<?php
				if($count >= 9):
					// Get the ID of a given category
					$term = get_term_by( 'slug', 'actualite', 'category' );
				    // Get the URL of this category
				    $category_link = get_term_link( $term );

					?>
					<p>
						<a href="<?php echo $category_link;?>page/2" class="button button-radius">
							Afficher plus
						</a>
					</p>
				<?php endif;?>
		</div>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
