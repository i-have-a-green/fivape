<?php get_header('extranet'); ?>
	<div id="pre-content">
		<div class="fil_ariane forum">
			<p>
				Forum
			</p>
		</div>
		<main id="content">
			<div id="pre-content">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<p>
					<h1><?php the_title();?></h1>
				</p>
				<p>
					<b><?php the_author( ); ?></b> Le <?php echo get_the_date();?>
				</p>
				<p>
					<b><?php comments_number( 'Pas de commentaire', '1 commentaire', '% commentaires' ); ?></b>
				</p>
				<?php the_content(); ?>
				<?php comments_template(); ?>
			<?php endwhile; endif; ?>
			</div>
		</main>
	</div>
<?php get_footer(); ?>
