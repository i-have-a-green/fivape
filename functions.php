<?php
// Theme support options
require_once(get_stylesheet_directory().'/include/theme-support.php');

// WP Head and other cleanup functions
require_once(get_stylesheet_directory().'/include/cleanup.php');

// Register scripts and stylesheets
require_once(get_stylesheet_directory().'/include/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_stylesheet_directory().'/include/menu.php');

// Register sidebars/widget areas
require_once(get_stylesheet_directory().'/include/sidebar.php');

// Makes WordPress comments suck less
require_once(get_stylesheet_directory().'/include/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_stylesheet_directory().'/include/page-navi.php');

// Adds support for multiple languages
//require_once(get_stylesheet_directory().'/assets/translation/translation.php');

// Remove 4.2 Emoji Support
require_once(get_stylesheet_directory().'/include/disable-emoji.php');

// ajouter des formats d'image
require_once(get_stylesheet_directory().'/include/add_image_size.php');

// Ajouter des options dans l'admin
require_once(get_stylesheet_directory().'/include/options.php');

// Use this as a template for custom post types
require_once(get_stylesheet_directory().'/include/custom-post-type.php');

// Customize the WordPress login menu
require_once(get_stylesheet_directory().'/include/login.php');

// Customize the WordPress admin
require_once(get_stylesheet_directory().'/include/admin.php');

require_once(get_stylesheet_directory().'/include/acf.php');

require_once(get_stylesheet_directory().'/include/plugin-require.php');

require_once(get_stylesheet_directory().'/include/role.php');

require 'vendor/autoload.php';
use \Mailjet\Resources;

/***************CONTACT*******************/
add_action( 'wp_ajax_formContact', 'formContact' );
add_action( 'wp_ajax_nopriv_formContact', 'formContact' );
function formContact(){
	if (wp_verify_nonce($_POST['nonceFormContact'], 'nonceFormContact')) {
		global $wpdb;

		$to = get_option( 'fivape_email_dest_contact' );
		$subject = "Contact depuis le site internet";
		$body = 'Nom : '.sanitize_text_field($_POST['name'])."\r\n";
		$body .= 'E-mail : '.sanitize_text_field($_POST['email'])."\r\n";
		$body .= 'Tel : '.sanitize_text_field($_POST['phone'])."\r\n";
		$body .= 'Sujet : '.sanitize_text_field($_POST['subject'])."\r\n";
		$body .= 'Commentaire : '.sanitize_textarea_field($_POST['comments'])."\r\n";
		$headers[] = 'From: Fivape <ne-pas-repondre@fivape.org>';
		wp_mail( $to, $subject, $body, $headers, $attachments);

        $post['post_type']   = 'contact';
        $post['post_status'] = 'publish';
		$post['post_title'] = sanitize_text_field($_POST['name']);
		$post['post_content'] = $body;
		wp_insert_post( $post, true );
	}
	die();
}

/***************FORMATION*******************/
add_action( 'wp_ajax_formFormation', 'formFormation' );
add_action( 'wp_ajax_nopriv_formFormation', 'formFormation' );
function formFormation(){
	if (wp_verify_nonce($_POST['nonceFormFormation'], 'nonceFormFormation')) {
		global $wpdb;

		$to = get_option( 'fivape_email_dest_formation' );
		$subject = "Contact Formation depuis le site internet";
		$body = 'Nom : '.sanitize_text_field($_POST['name'])."\r\n";
		$body .= 'E-mail : '.sanitize_text_field($_POST['email'])."\r\n";
		$body .= 'Tel : '.sanitize_text_field($_POST['phone'])."\r\n";
		$body .= 'Entreprise : '.sanitize_text_field($_POST['company'])."\r\n";
		$body .= 'Commentaire : '.sanitize_textarea_field($_POST['comments'])."\r\n";
		$headers[] = 'From: Fivape <ne-pas-repondre@fivape.org>';
		wp_mail( $to, $subject, $body, $headers, $attachments);

    $post['post_type']   = 'formation';
    $post['post_status'] = 'publish';
		$post['post_title'] = sanitize_text_field($_POST['name']);
		$post['post_content'] = $body;
		wp_insert_post( $post, true );
	}
	wp_die();
}
/***************Adhesions*******************/
add_action( 'wp_ajax_formAdhesion', 'formAdhesion' );
add_action( 'wp_ajax_nopriv_formAdhesion', 'formAdhesion' );
function formAdhesion(){
	if (wp_verify_nonce($_POST['nonceFormAdhesion'], 'nonceFormAdhesion')) {
		global $wpdb;

		$to = get_option( 'fivape_email_dest_adhesion' );
		$subject = "Adhesion depuis le site internet";
		$body = 'Société : '.sanitize_text_field($_POST['company'])."\r\n";
		$body .= 'Nom : '.sanitize_text_field($_POST['name'])."\r\n";
		$body .= 'Prénom : '.sanitize_text_field($_POST['firstname'])."\r\n";
		$body .= 'E-mail : '.sanitize_text_field($_POST['email'])."\r\n";
		$body .= 'Tel : '.sanitize_text_field($_POST['phone'])."\r\n";
		$headers[] = 'From: Fivape <ne-pas-repondre@fivape.org>';
		wp_mail( $to, $subject, $body, $headers, $attachments);

        $post['post_type']   = 'adhesion';
        $post['post_status'] = 'publish';
		$post['post_title'] = sanitize_text_field($_POST['company']);
		$post['post_content'] = $body;
		wp_insert_post( $post, true );
	}
	die();
}

add_filter('next_post_link', 'post_link_attributes');
add_filter('previous_post_link', 'post_link_attributes');

function post_link_attributes($output) {
    $injection = 'class="button button-white"';
    return str_replace('<a href=', '<a '.$injection.' href=', $output);
}

function link_email( $email = null ) {
	if ( ! is_email( $email ) ) {
		return;
	}
	$email = antispambot( $email );
	$email_link = sprintf( 'mailto:%s', $email );
	return sprintf( '<a href="%s">%s</a>', esc_url( $email_link, array( 'mailto' ) ), esc_html( $email ) );
}

add_action( 'save_post', 'patch_correct_tiret', 10, 3 );
function patch_correct_tiret( $post_id, $post, $update) {
	if(get_post_status( $post_id ) == 'publish' ){
  	if($post->post_type == 'adherent'){
			  $new_title = str_replace(array('—','–'),'-', $post->post_title);
        if ( $post->post_title !== $new_title ) {
          $post_update = array(
            'ID'         => $post->ID,
            'post_title' => $new_title
          );
          wp_update_post( $post_update );
        }
    }
  }
}

add_action( 'wp_insert_post', 'notification_new_post_extranet_send_email', 10, 3 );
function notification_new_post_extranet_send_email( $post_id, $post, $update) {

	// If this is a revision, don't send the email.
	if ( wp_is_post_revision( $post ) || get_post_status( $post_id ) == 'auto-draft' || get_post_status( $post_id ) == 'draft' ){
		return;
	}
	if(get_post_status( $post_id ) == 'publish' ){
		if($post->post_type == 'event'){
			$subject = 'Fivape événement : '.$post->post_title ;
			$message = '<h2>'.$post->post_title.'</h2>';
      $message .= get_field('nom_du_lieu').'<br />';
      $message .= nl2br($post->post_content);
      $message .= "<p>Lien de l'article : ".add_query_arg("redirect",home_url("/extranet/evenements/"),home_url())." </p>";
		}
		else if($post->post_type == 'forum'){
      $subject = 'Fivape forum : '.$post->post_title;
			$message = '<h2>'.get_the_date("j F Y", $post->ID).' - '.$post->post_title.' ('.get_the_author_meta( 'display_name' , $post->post_author ).') :</h2>';
			$message .= nl2br($post->post_content);
      $message .= "<p>Lien de l'article : ".add_query_arg("redirect",get_permalink( $post_id ),home_url())." </p>";
		}
		else if($post->post_type == 'doc'){
			$subject = 'Fivape document : '.$post->post_title;
			$message = '<h2>'.$post->post_title.'</h2>';
      $message .= nl2br($post->post_content);
      $message .= "<p>Lien de l'article : ".add_query_arg("redirect",home_url("/extranet/documents/"),home_url())." </p>";
		}
		else if($post->post_type == 'sondage'){
			$subject = 'Fivape sondage : '.$post->post_title;
			$message = '<h2>'.$post->post_title.'</h2>';
      $message .= "<p>Lien de l'article : ".add_query_arg("redirect",home_url("/extranet/sondage/"),home_url())." </p>";
		}
		else{
			return;
		}
    template_mail($message, $subject);
	}
}
function wpse27856_set_content_type(){
    return "text/html";
}
if ( ! function_exists( 'template_mail' ) ) {
  function template_mail($content, $title){
    $content = '
    <html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Fivape</title>
      <style type="text/css">
      #template_body a{
        color: #6db5db;
      }
      </style>
      <style type="text/css" id="custom-css">
      </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
      <div id="body" style="
      background-color:#6db5db;
      width:100%;
      margin:0;
      padding: 70px 0 70px 0;
      ">
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
          <td align="center" valign="top">
            <table border="0" cellpadding="0" cellspacing="0"  id="template_container" style="
            -webkit-border-radius:6px !important;
            border-radius:6px !important;
            background-color: #f4f4f4;
            border-radius:6px !important;
            max-width: 680px;">
              <tr>
                <td align="center" valign="top">
                  <!-- Header -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_header" style="
                  background-color: #f4f4f4;
                  color: #333;
                  -webkit-border-top-left-radius:6px !important;
                  -webkit-border-top-right-radius:6px !important;
                  border-top-left-radius:6px !important;
                  border-top-right-radius:6px !important;
                  border-bottom: 0;
                  font-family:Arial;
                  font-weight:bold;
                  line-height:100%;
                  vertical-align:middle;
                  ">
                    <tr>
                      <td>
                      <h1 style=" color: #333; margin:0; padding: 28px 24px; display:block; font-family:Arial; font-size: 30px; font-weight:bold; text-align:center; line-height: 150%; " id="logo">
                        <a href="'.home_url().'" style=" color: #333; text-decoration: none; ">
                          <img src="'.get_option('fivape_logo').'" alt="Fivape">
                        </a>
                      </h1>
                      </td>
                    </tr>
                  </table>
                  <!-- End Header -->
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <!-- Body -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_body">
                    <tr>
                      <td valign="top" style="
                      background-color: #fff;
                      " id="mailtpl_body_bg">
                        <!-- Content -->
                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                          <tr>
                            <td valign="top">
                              <div style="
                              color: #333;
                              font-family:Arial;
                              font-size: 14px;
                              line-height:150%;
                              text-align:left;
                              " id="mailtpl_body">
                                '.$content.'
                              </div>
                            </td>
                          </tr>
                        </table>
                        <!-- End Content -->
                      </td>
                    </tr>
                  </table>
                  <!-- End Body -->
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <!-- Footer -->
                  <table border="0" cellpadding="10" cellspacing="0" width="100%" id="template_footer" style="
                  border-top:1px solid #333;
                  background: #0077a7;
                  -webkit-border-radius:0px 0px 6px 6px;
                  -o-border-radius:0px 0px 6px 6px;
                  -moz-border-radius:0px 0px 6px 6px;
                  border-radius:0px 0px 6px 6px;
                  ">
                  <tr>
                    <td valign="top">
                      <table border="0" cellpadding="10" cellspacing="0" width="100%">
                        <tr>
                          <td colspan="2" valign="middle" id="credit" style="
                          border:0;
                          color: #333;
                          font-family: Arial;
                          font-size: 12px;
                          line-height:125%;
                          text-align:center;
                          ">

                            &copy;2018 Fivape<br />
                            <p>
                              Cet email a été envoyé à [[EMAIL_TO]], <a href="[[UNSUB_LINK_LOCALE]]" style="color: #333;">cliquer ici pour se désinscrire</a>
                            </p>
                            <p>

                            </p>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  </table>
                  <!-- End Footer -->
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
    </body>
    </html>
  ';

    $mj = new \Mailjet\Client('6407895bee192b1ed3fc0cea2d3d9c1b','dc3f956a81d4777733ba39a73fedff10');

    $body = [
        'Locale' => "fr_Fr",
        'Sender' => "Fivape",
        'SenderEmail' => "ne-pas-repondre@fivape.org",
        'Subject' => $title,
        'ContactsListID' => "2064",
        'Title' => $title
    ];
    $response = $mj->post(Resources::$Campaigndraft, ['body' => $body]);
    //$response->success() && var_dump($response->getData());
    $data = $response->getData();
    $id = $data[0]["ID"];
    $body = [
        'Html-part' => $content
    ];
    $response = $mj->post(Resources::$CampaigndraftDetailcontent, ['id' => $id, 'body' => $body]);
    //$response->success() && var_dump($response->getData());

    $response = $mj->post(Resources::$CampaigndraftSend, ['id' => $id]);
    //$response->success() && var_dump($response->getData());
  }
}

add_action( 'wp_insert_comment', 'notification_new_comment_extranet_send_email', 10, 3 );
function notification_new_comment_extranet_send_email( $id, $comment) {

	$post_id = $comment->comment_post_ID;
	$post = get_post($post_id);
	if($post->post_type == "forum"){
    $subject = 'Fivape forum : '.$post->post_title.' (commentaire de '.get_comment_author($id).')';
    //$comments = get_comments( array( 'post_id' => $post_id, 'order' => 'ASC' ) );
    $comments = get_comments( array( 'post_id' => $post_id ) );
    $message = "";
     foreach ( $comments as $comment ) :
        $message .= '<h3>'.get_comment_date( "j F Y", $comment->comment_ID ).' - Commentaire de '.$comment->comment_author.'</h3>';
        $message .= '<div style="font-style:italic !important;">'.nl2br($comment->comment_content).'</div>';
    endforeach;
    $message .= '<h2 style="margin-top:70px;">'.get_the_date("j F Y", $post->ID).' - '.$post->post_title.' ('.get_the_author_meta( 'display_name' , $post->post_author ).')</h2>';
    $message .= nl2br($post->post_content);
    $message .= '<p style="margin-top:70px;">'.add_query_arg("redirect",get_permalink( $post_id ),home_url()).'</p>';

    template_mail($message, $subject);
	}
}

add_action('wp_mail_failed', 'log_mailer_errors', 10, 1);
function log_mailer_errors(){
  $fn = ABSPATH . '/mail.log'; // say you've got a mail.log file in your server root
  $fp = fopen($fn, 'a');
  fputs($fp, "Mailer Error: " . $mailer->ErrorInfo ."\n");
  fclose($fp);
}

add_filter( 'comment_form_field_comment', 'wpse_64243_comment_editor' );
function wpse_64243_comment_editor( $field ) {
    $settings = array(
      'media_buttons' => false,
      'wpautop' => true,
      'quicktags' => false,
      'textarea_rows' => 15);
    if (!is_single()) return $field; //only on single post pages.
    global $post;
    ob_start();
    wp_editor( '', 'comment', $settings);
    $editor = ob_get_contents();
    ob_end_clean();
    //make sure comment media is attached to parent post
    $editor = str_replace( 'post_id=0', 'post_id='.get_the_ID(), $editor );
    return $editor;
}













// --------------------//
// ***** L'INUIT ***** //
// --------------------//



// IMAGES SIZES
add_image_size('news', 600, 480, true);
add_image_size('header', 1800, 900, true);
function my_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'header' => __( 'header (1800*900)' ),
        'news' => __( 'news (600*480)' )
    ) );
}
add_filter( 'image_size_names_choose', 'my_custom_sizes' );

// REGISTER WP MENU 
function register_linuit_menu(){
  register_nav_menus(array(
      'header-middle-v2' => 'v2 - Menu header middle',
      'header-bottom-v2' => 'v2 - Menu header bottom (principal)',
      'header-bottom-extranet-v2' => 'v2 - Menu header bottom extranet (principal)',
      'mobile-v2' => 'v2 - Menu mobile',
      'mobile-extranet-v2' => 'v2 - Menu mobile extranet',
      'footer-top-v2' => 'v2 - Menu footer top'
  ));
}
add_action('init', 'register_linuit_menu');


// WP MENU FUNCTION
function linuit_nav($themelocation,$depth = 0,$classUl){
  wp_nav_menu(
      array(
          'menu'            => '',
          'container'       => '',
          'container_class' => '',
          'container_id'    => '',
          'menu_class'      => '',
          'menu_id'         => '',
          'echo'            => true,
          'fallback_cb'     => 'wp_page_menu',
          'before'          => '',
          'after'           => '',
          'link_before'     => '',
          'link_after'      => '',
          'items_wrap'      => '<ul id="%1$s" class="%2$s '.$classUl.'">%3$s</ul>',
          'item_spacing'    => 'preserve',
          'depth'           => $depth,
          'walker'          => '',
          'theme_location'  => $themelocation,
      )
  );
}

add_action( 'user_register', 'ihag_synchro_mailjet',10);
add_action( 'deleted_user', 'ihag_synchro_mailjet',10 );
function ihag_synchro_mailjet(){
	$listId = 2064;
	$mj = new \Mailjet\Client('6407895bee192b1ed3fc0cea2d3d9c1b', 'dc3f956a81d4777733ba39a73fedff10');
	$filters = [
		'ContactsList' => $listId,
    'Limit' => 1000
	];
	$response = $mj->get(Resources::$Contact, ['filters' => $filters]);
	$listContactMailjet = $response->getData();
	
	$wpusers = get_users(array( 'fields' => array( 'user_email' ) ) );
	$wp_user_add = [];
	//recherche si le wpUser sont présents mailJet, si ok : on dépile userMailJet,  sinon on ajoute
	foreach ( $wpusers as $user ) {
		$wpUserFindInMailJet = false;
		foreach($listContactMailjet as $key => $contactMailJet){
			if(strtolower($user->user_email) == $contactMailJet['Email']){
				unset($listContactMailjet[$key]);
				$wpUserFindInMailJet = true;
				break;
			}
		}
		if(!$wpUserFindInMailJet){ 
			$wp_user_add[] = [
				'Email' => $user->user_email,
				'IsExcludedFromCampaigns' => "false",
				'Name' => $user->user_email,
				'Properties' => "object"
				];

      $bodyMail .= "add : ".$user->user_email."<br>";
		}
	}


	$body = [
		'Action' => "addnoforce",
		'Contacts' => $wp_user_add
	];
	$response = $mj->post(Resources::$ContactslistManagemanycontacts, ['id' => $listId, 'body' => $body]);

	$contactsMailJet = [];
	foreach($listContactMailjet as $key => $contactMailJet){
		$contactsMailJet[] = [
			'Email' => $contactMailJet['Email'],
		];
    $bodyMail .= "del : ".$contactMailJet['Email']."<br>";
	}
  
	$body = [
		'Contacts' => $contactsMailJet,
		'ContactsLists' => [
			[
				'Action' => "remove",
				'ListID' => $listId
			]
		]
	];
	$response = $mj->post(Resources::$ContactManagemanycontacts, ['body' => $body]);


  $to = get_option( 'fivape_email_dest_adhesion' );
  $subject = "synchro contact";
  $headers[] = 'From: Fivape <ne-pas-repondre@fivape.org>';
  wp_mail( $to, $subject, $bodyMail, $headers);

}




add_action('admin_menu', 'ihag_sub_menu_role');
function ihag_sub_menu_role() {
    add_submenu_page(
        'tools.php',
        'pb_tiret',
        'Problème tiret',
        'manage_options',
        'pb_tiret',
        'pb_tiret',
    );
}

function pb_tiret(){
	
			$args = array( 	'posts_per_page' => -1,
							'post_type' => 'adherent');
			$myposts = get_posts( $args );
			foreach ( $myposts as $adherent ) : 
        $new_title = str_replace(array('—','–'),'-', $adherent->post_title);
        if ( $adherent->post_title !== $new_title ) {
          $post_update = array(
            'ID'         => $adherent->ID,
            'post_title' => $new_title
          );
          wp_update_post( $post_update );
          echo '<p>'.$adherent->post_title .'->'. $new_title.'</p>';
        }
			endforeach;
      echo 'traitement des tirets terminé';
}