<?php get_header(); ?>
	<div id="pre-content">

			<h1 class="page-title"><?php echo single_cat_title( '', false );?></h1>
			<div id="actualite">
			<?php if (have_posts()) : while (have_posts()) : the_post();?>

				<!-- To see additional archive styles, visit the /parts directory -->
				<?php get_template_part( 'parts/loop', 'archive' ); ?>

			<?php endwhile; endif;?>
			</div>
			<?php joints_page_navi(); ?>

	</div>
<?php get_footer(); ?>
