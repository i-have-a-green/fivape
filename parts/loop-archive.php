<?php
$image = get_field('image_home');
$size = 'medium'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
  $img = wp_get_attachment_url( $image, $size );
}
else{
  $img = get_the_post_thumbnail_url(get_the_id(), "medium");
}
?>

<div class="actu-item" style="background-image:url(<?php echo $img;?>)">
	<div class="actu-item-content" style="background-color:<?php the_field('bg');?>">
		<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
		<!--<p>
			<?php the_excerpt();?>
		</p>-->
		<p>
			<a href="<?php the_permalink();?>" class="button button-border button-transparent" data-color="<?php the_field('bg');?>">
				Lire la suite
			</a>
		</p>
	</div>
</div>
