<div class="event-item">
	<div class="event-titre">
		<h2><?php the_title();?></h2>
		<time><?php the_field('date'); ?></time>
	</div>
	<div class="event-info">
		<?php
		$type = rp_get_category('type');
		if(!empty($type->name)):
		?>
		<i class="fa fa-star" aria-hidden="true"></i> <?php echo $type->name; ?><br />
		<?php endif;?>
		<?php if(!empty(get_field('nom_du_lieu'))):?>
			<i class="fa fa-map-marker" aria-hidden="true"></i>
			<?php
				if(!empty(get_field('lien_google_map'))){
					echo '<a href="'.get_field('lien_google_map').'" target="_blank">'.get_field('nom_du_lieu').'</a>';
				}
				else{
					the_field('nom_du_lieu');
				}
			?><br />
		<?php endif;?>
		<?php if(!empty(get_field('heure_debut'))):?>
			<i class="fa fa-clock-o" aria-hidden="true"></i> <?php the_field('heure_debut'); ?>
			<?php if(!empty(get_field('heure_fin'))):?>
				à <?php the_field('heure_fin'); ?>
			<?php endif;?>
			<br />
		<?php endif;?>
	</div>
	<div class="event-excerpt">
		<?php the_content();?>
	</div>
	<?php if(!empty(get_field('lien_dinscription'))):?>
	<div class="event-button">
		<a href="<?php the_field('lien_dinscription');?>" target="_blank" class="button button-border button-white">
			S'inscrire
		</a>
	</div>
	<?php endif;?>
</div>
