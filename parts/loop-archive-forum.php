<div class="forum-item">
	<div class="forum-titre">
		<a href="<?php the_permalink();?>">
			<?php the_title();?>
		</a>
	</div>
	<div class="forum-info">
		<i class="fa fa-user-o" aria-hidden="true"></i> <?php the_author(); ?><br />
		<i class="fa fa-commenting-o" aria-hidden="true"></i> <?php comments_number( 'Pas de commentaire', '1 commentaire', '% commentaires' ); ?><br />
		<i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo get_the_date(); ?><br />
	</div>
	<div class="forum-excerpt">
		<?php the_excerpt();?>
	</div>
	<div class="forum-button">
		<a href="<?php the_permalink();?>" class="button button-border button-white">
			Accéder au salon
		</a>
	</div>
</div>
