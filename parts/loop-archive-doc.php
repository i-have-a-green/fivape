<div class="doc-item">
	<div class="doc-titre">
		<h2><?php the_title();?></h2>
	</div>
	<div class="doc-info">
		<?php
		$wpseo_primary_term = new WPSEO_Primary_Term( 'categorie', get_the_id() );
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
		$term = get_term( $wpseo_primary_term );
		if(!empty($term->name)):
		?>
		Catégorie : <b><?php echo $term->name; ?></b><br />
		<?php endif;?>
	</div>
	<div class="doc-excerpt">
		<?php the_content();?>
	</div>
	<div class="doc-button">
		<p>
			<time><?php echo get_the_date(); ?></time>
		</p>
		<a href="<?php the_field('fichier');?>" download="<?php echo sanitize_file_name(get_the_title());?>">
			<?php
				if(get_field('type_de_fichier') == 'xls'){
					echo '<i class="fa fa-file-excel-o" aria-hidden="true"></i>';
				}
				elseif(get_field('type_de_fichier') == 'pdf'){
					echo '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>';
				}
				elseif(get_field('type_de_fichier') == 'doc'){
					echo '<i class="fa fa-file-word-o" aria-hidden="true"></i>';
				}
				elseif(get_field('type_de_fichier') == 'zip'){
					echo '<i class="fa fa-file-archive-o" aria-hidden="true"></i>';
				}
				elseif(get_field('type_de_fichier') == 'jpeg'){
					echo '<i class="fa fa-file-image-o" aria-hidden="true"></i>';
				}
			 ?>
		</a>
	</div>
</div>
