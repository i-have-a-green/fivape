<div class="sondage-item">
	<div class="sondage-date">
		<?php echo get_the_date(); ?>
	</div>
	<div class="sondage-titre">
		<a href="<?php the_permalink();?>">
			<?php the_title();?>
		</a>
	</div>
	<div class="sondage-excerpt">
		<?php the_field('description');?>
	</div>
	<div class="sondage-button">
		<a href="<?php the_permalink();?>" class="button button-border button-white">
			<?php if(get_field('statut_du_sondge') == "progress"): ?>
				Répondre au sondage
			<?php else: ?>
				Résultat du sondage
			<?php endif;?>
		</a>
	</div>
</div>
