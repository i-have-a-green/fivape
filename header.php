<?php
if( is_user_logged_in() && isset($_GET['redirect']) && !empty($_GET['redirect']) ){
  wp_redirect( urldecode($_GET['redirect']), 302);
  exit();
}

?>
<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">

		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" href="<?php echo get_option('fivape_favicon');?>" sizes="16x16" type="image/png">
		<?php wp_head(); ?>

		<!-- Drop Google Analytics here -->
		<!-- end analytics -->

	</head>

	<body <?php body_class(); ?>>
		<div class="" id="menuMobile">
			<div class="align-right">
				<button class="button button-very-small" id="times-menu" onclick="menuMobile()">
					<i class="fa fa-times" aria-hidden="true"></i>
				</button>
			</div>
			<?php echo joints_top_nav_mobile(); ?>
		</div>
		<div class="overlay" onclick="menuMobile()" id="menuOverlay"></div>
		<div class="modal" id="modalConnect">
			<div class="modal-header">
				<h2>Accès extranet</h2>
				<button class="button button-very-small" onclick="closeModal()">
					<i class="fa fa-times" aria-hidden="true"></i>
				</button>
			</div>
			<div class="modal-content">
				<?php echo rp_fivape_login();?>
			</div>
			<!--<div class="modal-footer">
				<p>
					<button class="button" onclick="closeModal()">
						<i class="fa fa-times" aria-hidden="true"></i> Fermer
					</button>
				</p>
			</div>-->
		</div>
		<div class="overlay" onclick="closeModal" id="modalOverlay"></div>
			<header class="header">
				<div class="wrapper">
					<div id="sub-header">
						<div class="right">
							<a href="<?php echo get_option('fivape_twitter');?>" target="_blank">
								<i class="fa fa-twitter" aria-hidden="true"></i>
							</a>
							<a href="<?php echo get_option('fivape_facebook');?>" target="_blank">
								<i class="fa fa-facebook" aria-hidden="true"></i>
							</a>
							<a href="mailto:<?php echo get_option('fivape_email_dest_contact');?>">
								Nous contacter
							</a>
						</div>
					</div>
					<div class="header-row">
						<div class="header-logo">
							<a href="<?php echo home_url();?>">
								<img src="<?php echo get_option('fivape_logo');?>" alt="Logo Fivape">
							</a>
						</div>
						<div class="header-menu">
							<?php joints_top_nav(); ?>
						</div>
						<div class="header-button">
							<a href="<?php echo home_url('/adhesion');?>" class="button button-radius button-blue button-medium">
								Rejoindre la FIVAPE
							</a>
							<button id="intranet-button" onclick="openModal(document.getElementById('modalConnect'));" class="button button-radius button-medium">
								Accès extranet
							</button>
						</div>
						<div class="header-button-tablette">
							<div class="header-button-mobile">
								<div id="burger-menu" onclick="menuMobile()" class="button button-very-small">
									<i class="fa fa-bars" aria-hidden="true"></i>
								</div>
								<div id="header-mobile-rs">
									<a href="">
										<i class="fa fa-twitter" aria-hidden="true"></i>
									</a>
									<a href="">
										<i class="fa fa-facebook" aria-hidden="true"></i>
									</a>
								</div>
							</div>
							<div>
								<a href="<?php echo home_url('/contact');?>" class="button button-radius button-blue button-very-small">
									<i class="fa fa-envelope-o" aria-hidden="true"></i>
								</a>
								<button id="intranet-button" onclick="openModal(document.getElementById('modalConnect'));" class="button button-radius button-blue  button-very-small">
									<i class="fa fa-lock" aria-hidden="true"></i>
								</button>
							</div>
						</div>
	 				</div>
				</div>
			</header> <!-- end .header -->
      <?php
      if( isset($_GET['redirect']) && !empty($_GET['redirect']) ): ?>
        <script>
          document.addEventListener("DOMContentLoaded", function(event) {
            openModal(document.getElementById('modalConnect'));
          });
        </script>
      <?php endif; ?>
