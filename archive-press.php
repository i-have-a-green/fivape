<?php get_header(); ?>
  <?php $p = get_page_by_path("media"); ?>
	<div id="pre-content">
		<?php get_template_part( 'parts/nbr', 'adherents' ); ?>
		<?php if(!empty(get_field('fil_ariane_', $p))):?>
			<div class="fil_ariane">
				<p>
					<?php if(!empty(get_field('picto_ariane', $p))):?>
						<?php the_field('picto_ariane', $p);?>
					<?php endif;?>
					<?php the_field('fil_ariane_', $p);?>
				</p>
			</div>

		<?php endif;?>
		<main id="content">
      <div>
  			<?php if (have_posts()) : while (have_posts()) : the_post();?>
        <a href="<?php the_field('lien');?>" target="_BLANK">
          <div class="press paragraphe">
            <div class="paragraphe-title">
              <h2><?php the_title();?></h2>
            </div>
            <div class="thumbnail">
              <?php the_post_thumbnail('medium'); ?>
            </div>
            <div class="content">
              <?php the_excerpt();?>
            </div>
          </div>
        </a>
			  <?php endwhile; endif;?>
		  </div>
		  <?php joints_page_navi(); ?>
    </main>
  </div>
<?php get_footer(); ?>
