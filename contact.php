<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div id="pre-content">
			<?php if ( has_post_thumbnail() ) :?>
					<div id="img-pre-content" style="background-image:url(<?php the_post_thumbnail_url( 'img-pre-content'); ?>)"></div>
			<?php endif;?>
			<div class="fil_ariane">
				<p>
					Nous contacter / Nous rejoindre
				</p>
			</div>
			<div id="content">
				<div id="cont-contact">
					<div>
						<p>
							<img src="<?php echo get_option('fivape_logo');?>" alt="logo footer" />
						</p>
						<p>
							Fivape, Fédération Interprofessionnelle de la Vape (Ex-CACE)
						</p>
						<p>
							<strong><u>Adresse :</u><br />
							Fivape<br />
							<?php echo get_option('fivape_adresse');?><br />
							<?php echo get_option('fivape_cp');?> <?php echo get_option('fivape_ville');?><br />
							</strong>
						</p>
						<?php if(!empty(get_option('fivape_email_bureau'))):?>
						<p>
							<strong><u>Bureau :</u> <?php echo link_email(get_option('fivape_email_bureau'));?></strong>
						</p>
						<?php endif;?>
						<?php if(!empty(get_option('fivape_email_adherent'))):?>
						<p>
							<strong><u>Adhésion :</u> <?php echo link_email(get_option('fivape_email_adherent'));?></strong>
						</p>
						<?php endif;?>
					</div>
					<div>
						<main>
							<form id="form-contact" action="" class="form-contact">
								<input type="hidden" name="action" value="formContact" />
								<?php wp_nonce_field('nonceFormContact', 'nonceFormContact'); ?>
								<p>
									<input type="text" name="name" id="name" required="required" placeholder="Nom/Prénom*" />
								</p>
								<p>
									<input type="email" name="email" id="email" required="required" placeholder="Email*" />
								</p>
								<p>
									<input type="tel" name="phone" id="phone" placeholder="Téléphone" />
								</p>
								<p>
									<input type="text" name="subjet" id="subjet" placeholder="Sujet (ex: informations, presse, adhésion...)" />
								</p>
								<p>
									<textarea name="comments" id="comments" required="required" placeholder="Message*" /></textarea>
								</p>
								<button type="submit" class="button button-gray button-radius" />Envoyer</button>
							</form>
						</main>
					</div>
				</div>

			</div>
		</div>


	<?php endwhile; endif; ?>

<?php get_footer(); ?>
