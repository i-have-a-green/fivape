<?php
/*
Template Name: Forum
*/
get_header('extranet'); ?>
<div id="pre-content">
	<div class="fil_ariane doubleActions forum">
		<p>
			<?php the_title();?>
		</p>
		<div class="right">
			<a href="<?php echo home_url('/extranet/creation-post-forum');?>">Ouvrir un sujet</a>
		</div>
	</div>
	<main id="">
		<form method="post" action="" id="formForum" class="formForum">
			<div>
				<label><b>Répertoire</b></label>
				<select name="repertoire">
					<option value="">
						Tous
					</option>
					<?php
					$terms = get_terms('repertoire', array('hide_empty' => true));

					foreach($terms as $term){
						$checked = false;
						if(isset($_POST['repertoire']) && $_POST['repertoire'] == $term->term_id){
							$checked = true;
						}
						echo '<option value="'.$term->term_id.'" '.selected( $checked, true, false ).'>
						'.$term->name.'
						</option>';
					}
					?>
				</select>
			</div>
			<div>
				<label><b>Recherche</b></label>
				<input type="text" placeholder="Entrer votre recherche" name="keywords" />
			</div>
			<div id="cont-button">
				<button type="submit" name="searchForm" class="button">Rechercher</button>
			</div>
			<div id="tags">
				<input type="hidden" value="<?php if(isset($_POST['tag']) ){echo $_POST['tag'];}?>" name="tag" id="tag">
				<?php
				$terms = get_terms('tag', array('hide_empty' => true));
					foreach($terms as $term){
						$checked = '';
						if(isset($_POST['tag']) && $_POST['tag'] == $term->term_id){
							$checked = 'active';
						}
						echo '<button data-value="'.$term->term_id.'" class="button button-radius tag '.$checked.'">
						'.$term->name.'
						</button>';
					}
					?>
				</select>
			</div>
		</form>
<?php
$args = contentInspirationArgs();

$wp_query = new WP_Query($args);
if( $wp_query->have_posts() ):
	while ( $wp_query->have_posts() ) : $wp_query->the_post();
		get_template_part( 'parts/loop', 'archive-forum' );
	endwhile;
endif;?>
<?php joints_page_navi(); ?>
	</main>
</div>
<?php get_footer('extranet');
function contentInspirationArgs(){
	global $query;
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$args =  array(
			'paged' => $paged,
			'page' => $paged,
			'post_type' => 'forum',
			'post_status' => 'publish',
		);
	if(isset($_POST['searchForm']))
	{
		if(!empty($_POST['keywords'])){
			$args['s'] = $_POST['keywords'];
		}

		if(!empty($_POST['repertoire']) && !empty($_POST['tag'])){
			$args['tax_query'] = array(
					'relation' => 'AND',
					array(	'taxonomy' => 'repertoire',
							'field'    => 'term_id',
							'terms'    => $_POST['repertoire']
						),
					array(	'taxonomy' => 'tag',
							'field'    => 'term_id',
							'terms'    => $_POST['tag']
						),
					);
			return $args;
		}
		elseif( !empty($_POST['repertoire']) ){
			$args['tax_query'] = array(
					array(	'taxonomy' => 'repertoire',
							'field'    => 'term_id',
							'terms'    => $_POST['repertoire']
						),
					);
			return $args;
		}
		elseif( !empty($_POST['tag']) ){
			$args['tax_query'] = array(
				array(	'taxonomy' => 'tag',
						'field'    => 'term_id',
						'terms'    => $_POST['tag']
					),
				);
			return $args;
		}
	}

	return $args;
}
