<?php
/*
Template Name: Formation
*/
?>
<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div id="pre-content">
			<?php get_template_part( 'parts/nbr', 'adherents' ); ?>
			<?php if ( has_post_thumbnail() ) :?>
					<div id="img-pre-content" style="background-image:url(<?php the_post_thumbnail_url( 'img-pre-content'); ?>)"></div>
			<?php endif;?>
			<?php if(!empty(get_field('fil_ariane_'))):?>
				<div class="fil_ariane">
					<p>
						<?php if(!empty(get_field('picto_ariane'))):?>
							<?php the_field('picto_ariane');?>
						<?php endif;?>
						<?php the_field('fil_ariane_');?>
					</p>
				</div>

			<?php endif;?>
			<main id="content">
				<?php if(!empty(get_field('titre'))):?>
					<h1><?php the_field('titre');?></h1>
				<?php endif;?>
				<?php the_field('paragraphe');?>
				<?php if( have_rows('paragraphes') ): ?>
				   <?php while ( have_rows('paragraphes') ) : the_row(); ?>
					   <div class="paragraphe">
						   <?php if(!empty(get_sub_field('titre'))): ?>
								<div class="paragraphe-title">
									<h2>
										<?php if(!empty(get_sub_field('icone'))): ?>
											<?php the_sub_field('icone');?>
										<?php endif;?>
										<?php the_sub_field('titre'); ?>
									</h2>
								</div>
							<?php endif;?>
							<div class="paragraphe-content"><?php the_sub_field('contenu'); ?></div>
							
					   </div>
				   <?php endwhile;?>
			   <?php endif;?>
			   <h2>Obtenir des informations sur les formations Fivape</h2>
			   <form id="form-formation" action="" class="form-formation">
					<input type="hidden" name="action" value="formFormation" />
					<?php wp_nonce_field('nonceFormFormation', 'nonceFormFormation'); ?>
					<p>
						<input type="text" name="name" id="name" required="required" placeholder="Nom/Prénom*" />
					</p>
					<p>
						<input type="email" name="email" id="email" required="required" placeholder="Email*" />
					</p>
					<p>
						<input type="tel" name="phone" id="phone" required="required" placeholder="Téléphone*" />
					</p>
					<p>
						<input type="text" name="company" id="company" required="required" placeholder="Entreprise*" />
					</p>
					<p>
						<textarea name="comments" id="comments" required="required" placeholder="Message*" ></textarea>
					</p>
					<p>
						<input type="checkbox" required id="case">
						<label>
							En cochant cette case, l’Utilisateur reconnaît avoir pris pleine connaissance de la Charte relative aux données personnelles et consent expressément à la collecte de ses données à caractère personnel pour les finalités décrites en détail dans la Charte susmentionnée, et notamment pour l’actualisation par de ses fichiers d’adhérent et la gestion des demandes de droit d'accès, de rectification, d'opposition, de limitation et de portabilité 
						</label>
					</p>
					<button type="submit" class="button button-gray button-radius" 	>Envoyer</button>
				</form>
			</main>
		</div>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
