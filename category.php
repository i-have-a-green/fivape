<?php get_header(); ?>

<main class="category">
	<div class="container">
		<div class="row">
			<!-- <div class="col-12">
				<h1><?php // echo single_cat_title( '', false );?></h1>
			</div> -->
			<?php 
				$currentCateg    = get_queried_object();
				$currentCategID = $currentCateg->term_id;
				$termsCateg = get_terms(array(
					'taxonomy' => 'category',
					'hide_empty' => true
				));
				if ( ! empty( $termsCateg ) && ! is_wp_error( $termsCateg ) ) : 
			?>
				<div class="col-12 filters">
					<?php 
						foreach ( $termsCateg as $term ){ ?>
							<?php if (get_queried_object_id() == $term->term_id): ?>
								<a href="<?php echo get_term_link($term->term_id,'category'); ?>" class="active btn"><?php echo $term->name ?></a>
							<?php else: ?>
								<a href="<?php echo get_term_link($term->term_id,'category'); ?>" class="btn"><?php echo $term->name ?></a>
							<?php endif; ?>
					<?php } ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="row">
		<?php if (have_posts()) : while (have_posts()) : the_post();?>

			<!-- To see additional archive styles, visit the /parts directory -->
			<?php get_template_part( 'parts/loop', 'archive' ); ?>

		<?php endwhile; endif;?>
		</div>
		<div class="row">
			<div class="col-12">
				<?php joints_page_navi(); ?>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
