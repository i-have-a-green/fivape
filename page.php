<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div id="pre-content">
			<?php get_template_part( 'parts/nbr', 'adherents' ); ?>
			<?php if ( has_post_thumbnail() ) :?>
					<div id="img-pre-content" style="background-image:url(<?php the_post_thumbnail_url( 'img-pre-content'); ?>)"></div>
			<?php endif;?>
			<?php if(!empty(get_field('fil_ariane_'))):?>
				<div class="fil_ariane">
					<p>
						<?php if(!empty(get_field('picto_ariane'))):?>
							<?php the_field('picto_ariane');?>
						<?php endif;?>
						<?php the_field('fil_ariane_');?>
					</p>
				</div>

			<?php endif;?>
			<main id="content">
				<?php if(!empty(get_field('titre'))):?>
					<h1><?php the_field('titre');?></h1>
				<?php endif;?>
				<?php the_field('paragraphe');?>

				<?php if( have_rows('paragraphes') ): ?>
				   <?php while ( have_rows('paragraphes') ) : the_row(); ?>
					   <div class="paragraphe">
						   <?php if(!empty(get_sub_field('titre'))): ?>
								<div class="paragraphe-title">
									<h2>
										<?php if(!empty(get_sub_field('icone'))): ?>
											<?php the_sub_field('icone');?>
										<?php endif;?>
										<?php the_sub_field('titre'); ?>
									</h2>
								</div>
							<?php endif;?>
							<div class="paragraphe-content"><?php the_sub_field('contenu'); ?></div>
					   </div>
				   <?php endwhile;?>
			   <?php endif;?>
			   <p>
				   <a href="<?php echo get_option('fivape_url_dest_adhesion');?>" class="button button-radius button-blue">
	   					Rejoindre la Fivape
	   				</a>
			   </p>
			</main>
		</div>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
