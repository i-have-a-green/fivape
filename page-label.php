<?php 
/* Template Name: Label Bleue */ 
get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div id="pre-content">
			<?php get_template_part( 'parts/nbr', 'adherents' ); ?>
			<?php if ( has_post_thumbnail() ) :?>
					<div id="img-pre-content" style="background-image:url(<?php the_post_thumbnail_url( 'img-pre-content'); ?>)"></div>
			<?php endif;?>

			<div class="fil_ariane">
				<p>
					<?php if(!empty(get_field('picto_ariane'))):?>
						<?php the_field('picto_ariane');?>
					<?php endif;?>
					<?php
					$args = array( 	'posts_per_page' => -1,
									'post_type' => 'adherent',
									'order'=> 'ASC',
									'orderby' => 'title',
									'meta_query' => array(
										array(
										  'key' => 'label',
										  'value' => '1',
										  'compare' => '=='
										)
									  )
								);
						
					if(isset($_POST['category']) && !empty($_POST['category']) ){
						$args['tax_query'] = array(
												array(
													'taxonomy' => 'activity',
													'field'    => 'slug',
													'terms'    => $_POST['category']
												));
					}
					if(isset($_POST['enseigne']) && !empty($_POST['enseigne']) ){
						$args['s'] = $_POST['enseigne'];
					}
					$myposts = get_posts( $args );
					?>
					LABEL VAPE BLEUE : <?php echo sizeof($myposts);?> points de ventes labellisés
				</p>
			</div>


			<main id="content">
				<?php if(!empty(get_field('titre'))):?>
					<h1><?php the_field('titre');?></h1>
				<?php endif;?>
				<?php the_field('paragraphe');?>

				<?php if( have_rows('paragraphes') ): ?>
				   <?php while ( have_rows('paragraphes') ) : the_row(); ?>
					   <div class="paragraphe">
						   <?php if(!empty(get_sub_field('titre'))): ?>
								<div class="paragraphe-title">
									<h2>
										<?php if(!empty(get_sub_field('icone'))): ?>
											<?php the_sub_field('icone');?>
										<?php endif;?>
										<?php the_sub_field('titre'); ?>
									</h2>
								</div>
							<?php endif;?>
							<div class="paragraphe-content"><?php the_sub_field('contenu'); ?></div>
					   </div>
				   <?php endwhile;?>
			   <?php endif;?>

			   <div id="pre-content">
				<h2>Recherche Boutiques Labellisées Vape Bleue :</h2>
				<form action="<?php the_permalink();?>" method="post" id="form-search">
					<label for="enseigne">Enseigne, ou ville, ou n° département</label>
					<input id="enseigne" name="enseigne" class="" type="text" placeholder="Enseigne, ou ville, ou n° département" value="<?php if(isset($_POST['enseigne'])){echo $_POST['enseigne'];} ?>">

					<!--<label for="category">Activité</label>
					<select id="category" name="category" value="">
						<option value="">
							Toutes les activités
						</option>
						<?php $terms = get_terms( array(
							'taxonomy' => 'activity',
							'hide_empty' => true,
						) );
						$category = "";
						if(isset($_POST['category'])){$category = $_POST['category'];}
						foreach ( $terms as $term ) : ?>
							<option value="<?php echo $term->slug;?>" <?php selected( $term->slug, $category ); ?>>
								<?php echo $term->name;?>
							</option>
						<?php endforeach;?>
					</select>-->
					<div class="cont-button">
						<button type="submit">Rechercher</button>
					</div>
				</form>
				<!--<input id="pac-input" class="controls" type="text" placeholder="Département, Ville, CP">-->
				<div class="acf-map">
					<?php
					$tab_posts = array();
					global $post;
					
					foreach ( $myposts as $post ) : setup_postdata( $post );
						/*$wpseo_primary_term = new WPSEO_Primary_Term( 'activity', get_the_id() );
						$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
						$term = get_term( $wpseo_primary_term );*/
						$terms = wp_get_post_terms($post->ID, 'activity', array("fields" => "names"));
						$location = get_field('coordonnees');
						$tab_posts_temp = '<div><h4>'.get_the_title().'</h4><p class="address">';
						foreach($terms as $term){
							$tab_posts_temp .= $term . ' - ';
						}
						if(!empty(get_field('site_web'))){
							$tab_posts_temp .= '<a href="'.get_field('site_web').'" target="_blank">Site web</a>';
						}
						$tab_posts_temp .= '</p></div><hr />';

						$tab_posts[] = $tab_posts_temp;
						?>
						<div class="marker" data-label="1" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
							<h4><?php the_title(); ?></h4>
							<?php if(!empty(get_field('adresse'))):?>
							<p class="address"><?php echo the_field('adresse'); ?></p>
							<?php endif;?>
							<?php if(!empty(get_field('site_web'))):?>
							<p>
								<a href="<?php the_field('site_web');?>" target="_blank">Site web</a>
							</p>
							<?php endif;?>
						</div>
					<?php endforeach;
					wp_reset_postdata();?>
				</div>

				<div id="membres">
					<h2>Les Boutiques Labellisées Vape Bleue </h2>
					<?php
					foreach ($tab_posts as $value) {
						echo $value;
					}
					?>
				</div>

			</div>

			   <p>
				   <a href="<?php echo get_option('fivape_url_dest_adhesion');?>" class="button button-radius button-blue">
	   					Rejoindre la Fivape
	   				</a>
			   </p>
			</main>
		</div>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
