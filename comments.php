<?php
	if ( post_password_required() ) {
		return;
	}
?>

<div id="comments" class="comments-area">

	<?php // You can start editing here ?>

	<?php if ( have_comments() ) : ?>

		<ul class="commentlist">
			<?php wp_list_comments('type=comment&callback=joints_comments'); ?>
		</ul>

	<?php endif; // Check for have_comments(). ?>

	<?php comment_form(array(
		'class_submit'	=>'button',
		'logged_in_as'	=> '',
		'comment_field' => '<p class="comment-form-comment"><textarea id="comment" name="comment" style="width:100%;height:200px" aria-required="true"></textarea></p>',
));
		?>

</div><!-- #comments -->
