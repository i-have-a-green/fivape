<?php
if(! is_user_logged_in() ){
	wp_redirect(home_url(), 302);
	exit();
}
?>

<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">

		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" href="<?php echo get_option('fivape_favicon');?>" sizes="16x16" type="image/png">
		<?php wp_head(); ?>

		<!-- Drop Google Analytics here -->
		<!-- end analytics -->

	</head>

	<body <?php body_class(); ?>>
		<div class="" id="menuMobile">
			<div class="align-right">
				<button class="button button-very-small" id="times-menu" onclick="menuMobile()">
					<i class="fa fa-times" aria-hidden="true"></i>
				</button>
			</div>
			<?php echo joints_top_nav_mobile('main-nav-extranet'); ?>
		</div>
		<div class="overlay" onclick="menuMobile()" id="menuOverlay"></div>
		<div class="overlay" onclick="closeModal" id="modalOverlay"></div>
			<header class="header header-extranet">
				<div class="wrapper">
					<div id="sub-header" class="extra-sub-header">
						<a href="<?php echo wp_logout_url( home_url() );?>" class="float-left">
							Se déconnecter
						</a>

						<a href="<?php echo get_option('fivape_twitter');?>" target="_blank">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
						<a href="<?php echo get_option('fivape_facebook');?>" target="_blank">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
						<a href="<?php echo home_url('/contact');?>">
							Nous contacter
						</a>

					</div>
					<div class="header-row">
						<div class="header-logo">
							<a href="<?php echo home_url('extranet');?>">
								<img src="<?php echo get_option('fivape_logo');?>" alt="Logo Fivape">
							</a>
						</div>
						<div class="header-menu">
							<?php joints_top_nav('main-nav-extranet'); ?>
						</div>
						<div class="header-button-extranet">
							<!--<a href="<?php echo wp_logout_url( home_url() );?>" class="button button-radius button-blue button-medium">
								Se déconnecter
							</a>-->
							<div class="header-button-tablette">
								<div class="header-button-mobile">
									<div id="burger-menu" onclick="menuMobile()" class="button button-very-small">
										<i class="fa fa-bars" aria-hidden="true"></i>
									</div>
								</div>
							</div>
							<div class="form_search">
								<form method="post" action="<?php echo home_url('extranet/recherche');?>">
									<input type="text" name="search" placeholder="Votre recherche" />
									<button type="submit" class="button button-very-small button-radius"><i class="fa fa-search" aria-hidden="true"></i></button>
								</form>
							</div>
						</div>
							<!--<div>
								<a href="<?php echo wp_logout_url( home_url() );?>" class="button button-radius button-blue button-very-small">
									<i class="fa fa-power-off" aria-hidden="true"></i>
								</a>
							</div>-->
						</div>
	 				</div>
				</div>
			</header> <!-- end .header -->
