			<footer class="footer" role="contentinfo">
				<div class="wrapper">
					<div id="inner-footer">
						<div class="logo-footer">
							<img src="<?php echo get_option('fivape_logo_footer');?>" alt="logo Fivape">
							<p id="footer-adresse">
								<?php echo get_option('fivape_adresse');?><br />
								<?php echo get_option('fivape_cp');?>
								<?php echo get_option('fivape_ville');?>
							</p>
						</div>
						<div class="nav-footer">
							<nav role="navigation">
								<?php joints_footer_links('footer-links-extranet'); ?>
							</nav>
							<div class="rs-footer">
								<span>REJOIGNEZ NOUS SUR LES Réseaux sociaux</span>
								<a href="<?php echo get_option('fivape_twitter');?>" target="_blank">
									<i class="fa fa-twitter" aria-hidden="true"></i>
								</a>
								<a href="<?php echo get_option('fivape_facebook');?>" target="_blank">
									<i class="fa fa-facebook" aria-hidden="true"></i>
								</a>
							</div>
							<div class="source-org copyright">
								<a href="<?php echo home_url('mentions-legales');?>">
									&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>
								</a>
							</div>
						</div>
						<div class="prisme">
							<img src="<?php echo get_option('fivape_prisme');?>" alt="illustration">
						</div>
					</div> <!-- end #inner-footer -->
				</div><!-- end #wrapper -->
			</footer> <!-- end .footer -->
			<div class="modal" id="modalTest">
				<div class="modal-header">
					<h2>Formulaire de contact</h2>
					<button class="button button-very-small" onclick="closeModal()">
						<i class="fa fa-times" aria-hidden="true"></i>
					</button>
				</div>
				<div class="modal-content">
					
				</div>
				<div class="modal-footer">
					<p>
						<button class="button" onclick="closeModal()">
							<i class="fa fa-times" aria-hidden="true"></i> Fermer
						</button>
					</p>
				</div>
			</div>
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->
