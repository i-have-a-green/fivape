<?php
/*
Template Name: Adhesion
*/

wp_redirect( "http://adherent.fivape.org/data/modules/organisation/public/php/demande_adhesion.php", 302 )
?>
<?php get_header(); ?>
<script>
location.href="http://adherent.fivape.org/data/modules/organisation/public/php/demande_adhesion.php";
</script>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div id="pre-content">
			<?php if ( has_post_thumbnail() ) :?>
					<div id="img-pre-content" style="background-image:url(<?php the_post_thumbnail_url( 'img-pre-content'); ?>)"></div>
			<?php endif;?>
			<div class="fil_ariane">
				<p>
					Nous rejoindre
				</p>
			</div>
			<div id="content">
				<div id="cont-contact">
					<div>
						<p>
							<img src="<?php echo get_option('fivape_logo');?>" alt="logo footer" />
						</p>
						<p>
							Fivape, Fédération Interprofessionnelle de la Vape (Ex-CACE)
						</p>
						<p>
							<strong><u>Adresse :</u><br />
							Fivape<br />
							<?php echo get_option('fivape_adresse');?><br />
							<?php echo get_option('fivape_cp');?> <?php echo get_option('fivape_ville');?><br />
							</strong>
						</p>
						<?php if(!empty(get_option('fivape_email_bureau'))):?>
						<p>
							<strong><u>Bureau :</u> <?php echo link_email(get_option('fivape_email_bureau'));?></strong>
						</p>
						<?php endif;?>
						<?php if(!empty(get_option('fivape_email_adherent'))):?>
						<p>
							<strong><u>Adhésion :</u> <?php echo link_email(get_option('fivape_email_adherent'));?></strong>
						</p>
						<?php endif;?>
					</div>
					<div>
						<main>
							<form id="form-adhesion" action="" class="form-adhesion">
								<input type="hidden" name="action" value="formAdhesion" />
								<?php wp_nonce_field('nonceFormAdhesion', 'nonceFormAdhesion'); ?>
								<p>
									<input type="text" name="company" id="company" required="required" placeholder="Nom de la société*" />
								</p>
								<p>
									<input type="text" name="name" id="name" required="required" placeholder="Nom*" />
								</p>
								<p>
									<input type="text" name="firstname" id="firstname" required="required" placeholder="Prénom*" />
								</p>
								<p>
									<input type="tel" name="phone" id="phone" required="required" placeholder="Téléphone" />
								</p>
								<p>
									<input type="email" name="email" id="email" required="required" placeholder="Email*" />
								</p>
								<button type="submit" class="button button-gray button-radius" />Envoyer</button>
							</form>
						</main>
					</div>
				</div>

			</div>
		</div>


	<?php endwhile; endif; ?>

<?php get_footer(); ?>
