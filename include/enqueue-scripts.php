<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    //add jquery on the footer
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', get_stylesheet_directory_uri() . '/assets/js/jquery.min.js', false, NULL, true );
    wp_enqueue_script( 'jquery' );

    // Adding scripts file in the footer
    wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/assets/js/script.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'menuMobile', get_stylesheet_directory_uri() . '/assets/js/menuMobile.min.js', array( 'script' ), '', true );
    wp_localize_script('script', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
	wp_localize_script('script', 'dirImg', get_stylesheet_directory_uri() . '/assets/img/' );

	//stylesheet
	wp_enqueue_style( 'normalize', get_stylesheet_directory_uri() . '/assets/css/normalize.min.css', array(), '', 'all' );
	wp_enqueue_style( 'awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array('normalize'), '', 'all' );
	wp_enqueue_style( 'global', get_stylesheet_directory_uri() . '/assets/css/global.min.css', array('awesome'), '', 'all' );
	wp_enqueue_style( 'header', get_stylesheet_directory_uri() . '/assets/css/header.min.css', array('global'), '', 'all' );
	wp_enqueue_style( 'footer', get_stylesheet_directory_uri() . '/assets/css/footer.min.css', array('global'), '', 'all' );


    if ( is_page_template('adherents.php') || is_page_template('page-label.php')) {
		wp_enqueue_style( 'adherents', get_stylesheet_directory_uri() . '/assets/css/adherents.min.css', array('global'), '', 'all' );
		wp_enqueue_script( 'googleapis', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDFKqR7J4PrQ2JurolHm-CO3CuFrCqmLz4&libraries=places', array('adherents'), '', 'all' );
		wp_enqueue_script( 'adherents', get_stylesheet_directory_uri() . '/assets/js/adherents.js', array( 'script' ), '', true );
    }
	elseif ( is_page_template('contact.php') || is_page_template('adhesion.php') || is_page( 'formation' ) ) {
		wp_enqueue_script( 'contact', get_stylesheet_directory_uri() . '/assets/js/contact.min.js', array( 'script' ), '', true );
		wp_enqueue_style( 'contact', get_stylesheet_directory_uri() . '/assets/css/contact.min.css', array('header'), '', 'all' );
    }
	elseif ( is_page_template('extranet.php') || is_page_template('search-extranet.php') ) {
		wp_enqueue_style( 'forum', get_stylesheet_directory_uri() . '/assets/css/forum.min.css', array('header'), '', 'all' );
		wp_enqueue_style( 'doc', get_stylesheet_directory_uri() . '/assets/css/doc.min.css', array('header'), '', 'all' );
		wp_enqueue_style( 'event', get_stylesheet_directory_uri() . '/assets/css/event.min.css', array('header'), '', 'all' );
		wp_enqueue_style( 'sondage', get_stylesheet_directory_uri() . '/assets/css/sondage.min.css', array('header'), '', 'all' );
		wp_enqueue_style( 'extranet', get_stylesheet_directory_uri() . '/assets/css/extranet.min.css', array('sondage'), '', 'all' );
    }
	elseif ( is_page_template('forum.php') || is_page_template('create-forum.php') || is_singular( 'forum' ) ) {
		wp_enqueue_script( 'forum', get_stylesheet_directory_uri() . '/assets/js/forum.min.js', array( 'script' ), '', true );
		wp_enqueue_style( 'forum', get_stylesheet_directory_uri() . '/assets/css/forum.min.css', array('header'), '', 'all' );
    }
	elseif ( is_page_template('doc.php') || is_page_template('create-doc.php')) {
		wp_enqueue_style( 'doc', get_stylesheet_directory_uri() . '/assets/css/doc.min.css', array('header'), '', 'all' );
    }
	elseif ( is_page_template('event.php') ) {
		wp_enqueue_style( 'event', get_stylesheet_directory_uri() . '/assets/css/event.min.css', array('header'), '', 'all' );
    }
	elseif ( is_page_template('sondage.php') || is_singular( 'sondage' )  ) {
		wp_enqueue_style( 'sondage', get_stylesheet_directory_uri() . '/assets/css/sondage.min.css', array('header'), '', 'all' );
    }
	/*
	@url https://wordpress.stackexchange.com/questions/149803/conditional-tag-to-check-if-page-php-is-being-used
	*/

	elseif (is_front_page() || is_archive() ) {
		wp_enqueue_script( 'front-page', get_stylesheet_directory_uri() . '/assets/js/front-page.min.js', array( 'script' ), '', true );
		wp_enqueue_style( 'front-page', get_stylesheet_directory_uri() . '/assets/css/front-page.min.css', array('header'), '', 'all' );
	}
	elseif (is_single() ) {
		wp_enqueue_style( 'single', get_stylesheet_directory_uri() . '/assets/css/single.min.css', array('header'), '', 'all' );
	}
	elseif ( basename(get_page_template()) === 'page.php' || basename(get_page_template()) === 'formation.php') {
		wp_enqueue_style( 'page', get_stylesheet_directory_uri() . '/assets/css/page.min.css', array('header'), '', 'all' );
    }
	elseif ( basename(get_page_template()) === 'presentation.php') {
		wp_enqueue_style( 'presentation', get_stylesheet_directory_uri() . '/assets/css/presentation.min.css', array('header'), '', 'all' );
		}
	
	if(basename(get_page_template()) === 'formation.php') {
		wp_enqueue_style( 'page', get_stylesheet_directory_uri() . '/assets/css/page.min.css', array('header'), '', 'all' );
  }

}
add_action('wp_enqueue_scripts', 'site_scripts', 999);

/**
*   Child page conditional
*   @ Accept's page ID, page slug or page title as parameters
*/
function is_child( $parent = '' ) {
    global $post;

    @$parent_obj = get_page( $post->post_parent, ARRAY_A );
    $parent = (string) $parent;
    $parent_array = (array) $parent;

    if ( in_array( (string) $parent_obj['ID'], $parent_array ) ) {
        return true;
    } elseif ( in_array( (string) $parent_obj['post_title'], $parent_array ) ) {
        return true;
    } elseif ( in_array( (string) $parent_obj['post_name'], $parent_array ) ) {
        return true;
    } else {
        return false;
    }
}
