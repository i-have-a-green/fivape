<?php

add_action( 'admin_init', 'myThemeRegisterSettings' );

function myThemeRegisterSettings( )
{
	register_setting( 'my_theme', 'fivape_adresse' );
	register_setting( 'my_theme', 'fivape_cp' );
	register_setting( 'my_theme', 'fivape_ville' );
	register_setting( 'my_theme', 'fivape_email' );

	register_setting( 'my_theme', 'fivape_email_bureau' );
	register_setting( 'my_theme', 'fivape_email_adherent' );
	register_setting( 'my_theme', 'fivape_email_dest_adhesion' );
	register_setting( 'my_theme', 'fivape_url_dest_adhesion' );
	register_setting( 'my_theme', 'fivape_email_dest_contact' );
	register_setting( 'my_theme', 'fivape_email_dest_formation' );


	register_setting( 'my_theme', 'fivape_facebook' );
	register_setting( 'my_theme', 'fivape_twitter' );
	register_setting( 'my_theme', 'fivape_ville' );

	register_setting( 'my_theme', 'fivape_logo' );
	register_setting( 'my_theme', 'fivape_logo_footer' );
	register_setting( 'my_theme', 'fivape_prisme' );
	register_setting( 'my_theme', 'fivape_favicon' );

}

// la fonction myThemeAdminMenu( ) sera exécutée
// quand WordPress mettra en place le menu d'admin
add_action( 'admin_menu', 'myThemeAdminMenu' );
function myThemeAdminMenu( )
{
	add_menu_page(
		'Options de mon thème', // le titre de la page
		'Mon thème',            // le nom de la page dans le menu d'admin
		'administrator',        // le rôle d'utilisateur requis pour voir cette page
		'my-theme-page',        // un identifiant unique de la page
		'myThemeSettingsPage'   // le nom d'une fonction qui affichera la page
	);
}

function myThemeSettingsPage( )
{
	if(function_exists( 'wp_enqueue_media' )){
		wp_enqueue_media();
	}else{
		wp_enqueue_style('thickbox');
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
	}
?>
	<div class="wrap">
		<h2>Options de mon thème</h2>

		<form method="post" action="options.php">
			<input type="hidden" name="action" value="update" />
			<?php
				wp_nonce_field('update-options');
				settings_fields( 'my_theme' );
			?>


				<?php add_option_text('fivape_adresse', 'Adresse');?>
				<?php add_option_text('fivape_cp', 'Code postal');?>
				<?php add_option_text('fivape_ville', 'Ville');?>
				<?php add_option_text('fivape_email', 'Adresse email');?>

				<?php add_option_text('fivape_email_bureau', 'Adresse email bureau');?>
				<?php add_option_text('fivape_email_adherent', 'Adresse email adhérent');?>

				<?php add_option_text('fivape_email_dest_contact', 'Adresse email de destination de form de contact');?>
				<?php add_option_text('fivape_email_dest_adhesion', 'Adresse email destination pour les adhésions');?>
				<?php add_option_text('fivape_email_dest_formation', 'Adresse email destination pour les formations');?>
				<?php add_option_text('fivape_url_dest_adhesion', 'URL destination pour les adhésions');?>

				<?php add_option_text('fivape_facebook', 'Lien Facebook');?>
				<?php add_option_text('fivape_twitter', 'Lien Twitter');?>

				<?php add_option_image('fivape_logo', 'Logo Header');?>
				<?php add_option_image('fivape_logo_footer', 'Logo Footer');?>
				<?php add_option_image('fivape_prisme', 'Logo Prisme');?>
				<?php add_option_image('fivape_favicon', 'Favicon');?>
			</table>
			<p class="submit">
				<input type="submit" class="button-primary" value="Mettre à jour" />
			</p>
		</form>
	</div>
<?php
}


function add_option_text($name, $label){
	echo '	<table class="form-table"><tr valign="top">
				<th scope="row"><label for="'.$name.'">'.$label.'</label></th>
				<td><input type="text" id="'.$name.'" name="'.$name.'" class="regular-text" value="'.get_option( $name ).'" /></td>
			</tr>';
}
function add_option_image($name, $label){
	echo '	<tr valign="top">
				<th scope="row"><label for="'.$name.'">'.$label.'</label></th>
				<td>';

		echo '<img class="'.$name.'" src="'.get_option($name).'" style="max-height:100px;"/>';

	echo '<input class="'.$name.'_url" type="hidden" id="'.$name.'" name="'.$name.'" size="60" value="'.get_option($name).'">
			<button class="'.$name.'_upload button">Selectionner un fichier</button>
				</td>
			</tr>


			<script>
				jQuery(document).ready(function($) {
					$(".'.$name.'_upload").click(function(e) {
						e.preventDefault();
						var custom_uploader = wp.media({
							multiple: false  // Set this to true to allow multiple files to be selected
						})
						.on("select", function() {
							var attachment = custom_uploader.state().get("selection").first().toJSON();
							$(".'.$name.'").attr("src", attachment.url);
							$("#'.$name.'").val(attachment.url);
						})
						.open();
					});
				});
			</script>
	';
}
