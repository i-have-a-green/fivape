<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/

// adding the function to the Wordpress init
add_action( 'init', 'custom_post_forum');
// let's create the function for the custom type
function custom_post_forum() {
	register_post_type( 'adherent', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Adhérents', 'jointswp'), /* This is the Title of the Group */
				'singular_name' 	=> __('Adhérent', 'jointswp'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-businessman', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			//'rewrite'           => array( 'slug' => 'forum' ),
			'supports' 			=> array( 'title')
	 	) /* end of options */
	);

	// now let's add custom categories (these act like categories)
	register_taxonomy( 'activity',
		array('adherent'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Activités', 'jointswp' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Activité', 'jointswp' ), /* single taxonomy name */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,

		)
	);

	register_post_type( 'contact', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Contacts', 'jointswp'), /* This is the Title of the Group */
				'singular_name' 	=> __('Contact', 'jointswp'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-email-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			//'rewrite'           => array( 'slug' => 'forum' ),
			'supports' 			=> array( 'title', 'author', 'excerpt', 'editor', 'comments')
	 	) /* end of options */
	);

	register_post_type( 'formation', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Contact formation', 'jointswp'), /* This is the Title of the Group */
				'singular_name' 	=> __('Contacts formations', 'jointswp'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-awards', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			//'rewrite'           => array( 'slug' => 'forum' ),
			'supports' 			=> array( 'title', 'editor')
	 	) /* end of options */
	);
	

	register_post_type( 'adhesion', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Adhesions', 'jointswp'), /* This is the Title of the Group */
				'singular_name' 	=> __('Adhesion', 'jointswp'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 19, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-email-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			//'rewrite'           => array( 'slug' => 'forum' ),
			'supports' 			=> array( 'title', 'author', 'excerpt', 'editor', 'comments')
	 	) /* end of options */
	);

	// creating (registering) the custom type
	register_post_type( 'forum', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Forum', 'jointswp'), /* This is the Title of the Group */
				'singular_name' 	=> __('forum', 'jointswp'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-format-chat', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			//'rewrite'           => array( 'slug' => 'forum' ),
			'supports' 			=> array( 'title', 'author', 'excerpt', 'editor', 'comments')
	 	) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type('repertoire', 'forum');
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type('tag', 'inspiration');



	register_post_type( 'event', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Evénements', 'jointswp'), /* This is the Title of the Group */
				'singular_name' 	=> __('Evénement', 'jointswp'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 9, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-calendar-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			//'rewrite'           => array( 'slug' => 'forum' ),
			'supports' 			=> array( 'title', 'editor', 'custom-fields')
	 	) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type('type', 'event');

	register_post_type( 'doc', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Documents', 'jointswp'), /* This is the Title of the Group */
				'singular_name' 	=> __('Document', 'jointswp'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 10, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-welcome-add-page', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			//'rewrite'           => array( 'slug' => 'forum' ),
			'supports' 			=> array( 'title', 'editor', 'custom-fields', 'thumbnail')
	 	) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	register_taxonomy_for_object_type('categorie', 'doc');

	register_post_type( 'sondage', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('Sondages', 'jointswp'), /* This is the Title of the Group */
				'singular_name' 	=> __('Sondage', 'jointswp'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 11, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-groups', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'supports' 			=> array( 'title', 'editor', 'custom-fields', 'thumbnail')
	 	) /* end of options */
	); /* end of register post type */
}

// now let's add custom categories (these act like categories)
register_taxonomy( 'repertoire',
	array('forum'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Repertoires', 'jointswp' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Répertoire', 'jointswp' ), /* single taxonomy name */
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,

	)
);
/*
for more information on taxonomies, go here:
http://codex.wordpress.org/Function_Reference/register_taxonomy
*/

// now let's add custom tags (these act like categories)
register_taxonomy( 'tag',
	array('forum'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	array('hierarchical' => false,    /* if this is false, it acts like tags */
		'labels' => array(
			'name' => __( 'Tags', 'jointswp' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Tag', 'jointswp' ) /* single taxonomy name */
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
	)
);

// now let's add custom categories (these act like categories)
register_taxonomy( 'type',
	array('event'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Types d\'événement', 'jointswp' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Type d\'événement', 'jointswp' ), /* single taxonomy name */
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,

	)
);

// now let's add custom categories (these act like categories)
register_taxonomy( 'categorie',
	array('doc'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Catégories', 'jointswp' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Catégorie', 'jointswp' ), /* single taxonomy name */
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
	)
);

register_post_type( 'press', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
  // let's now add all the options for this post type
  array('labels' 			=> array(
      'name' 				=> __('Presse', 'jointswp'), /* This is the Title of the Group */
      'singular_name' 	=> __('Presse', 'jointswp'), /* This is the individual type */
    ), /* end of arrays */
    'menu_position' 	=> 11, /* this is what order you want it to appear in on the left hand side menu */
    'menu_icon' 		=> 'dashicons-format-aside', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
    'hierarchical' 		=> false,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite' => array('slug'=>'presse'),
    'has_archive' => true,
    'supports' 			=> array( 'title', 'excerpt', 'thumbnail')
  ) /* end of options */
); /* end of register post type */
