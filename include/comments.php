<?php
// Comment Layout
function joints_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class('panel'); ?>>
		<div class="media-object">
			<div class="media-object-section">
				<article id="comment-<?php comment_ID(); ?>">
					<header class="comment-author">
						<?php //printf(__('%s', 'jointswp'), get_comment_author_link()) ?>
						<?php echo get_comment_author();//the_author( ); ?>
						<time datetime="<?php echo comment_time('Y-m-j'); ?>">
							<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
								Le <?php comment_time(__('j F Y', 'jointswp')); ?>
							</a>
						</time>
						<!-- <?php edit_comment_link(__('(Edit)', 'jointswp'),'  ','') ?> -->
					</header>
					<section class="comment_content clearfix">
						<?php comment_text() ?>
					</section>
				</article>
			</div>
		</div>
	<!-- </li> is added by WordPress automatically -->
<?php
}
