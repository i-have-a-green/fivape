<?php

add_filter('show_admin_bar', '__return_false');

add_action( 'admin_init', 'customRoleAdmin' );
function customRoleAdmin(){
	global $current_user;
	wp_get_current_user();
	if ( user_can( $current_user, "bureaufivape" ) ){
		remove_menu_page( 'tools.php' );
		remove_menu_page( 'users.php' );
	}
	elseif ( user_can( $current_user, "conseilfivape" ) ){
		remove_menu_page( 'tools.php' );
		remove_menu_page( 'edit.php?post_type=page' );
		remove_menu_page( 'edit.php?post_type=adherent' );
		remove_menu_page( 'edit.php?post_type=contact' );
		remove_menu_page( 'edit.php?post_type=adhesion' );
		remove_menu_page( 'users.php' );
	}
	elseif ( user_can( $current_user, "forumfivape" ) ){
		remove_menu_page( 'tools.php' );
		remove_menu_page( 'edit.php?post_type=page' );
		remove_menu_page( 'edit.php' );
		remove_menu_page( 'edit.php?post_type=adherent' );
		remove_menu_page( 'edit.php?post_type=contact' );
		remove_menu_page( 'edit.php?post_type=adhesion' );
		remove_menu_page( 'users.php' );
	}
}
