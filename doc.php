<?php
/*
Template Name: Doc
*/
get_header('extranet'); ?>
<div id="pre-content">
	<div class="fil_ariane doubleActions doc">
		<p>
			<?php the_title();?>
		</p>
		<div class="right">
			<!--<a href="<?php echo home_url('/extranet/creation-doc');?>">Ajouter un document</a>-->
		</div>
	</div>
	<main id="content">
		<form method="post" action="" id="formDoc" class="formDoc">
			<div>
				<label><b>Catégorie</b></label>
				<select name="categorie">
					<option value="">
						Toutes
					</option>
					<?php
					$terms = get_terms('categorie', array('hide_empty' => true));

					foreach($terms as $term){
						$checked = false;
						if(isset($_POST['categorie']) && $_POST['categorie'] == $term->term_id){
							$checked = true;
						}
						echo '<option value="'.$term->term_id.'" '.selected( $checked, true, false ).'>
						'.$term->name.'
						</option>';
					}
					?>
				</select>
			</div>
			<div>
				<label><b>Recherche</b></label>
				<input type="text" placeholder="Entrer votre recherche" name="keywords" />
			</div>
			<div id="cont-button">
				<button type="submit" name="searchFormDoc" class="button">Rechercher</button>
			</div>
		</form>
<?php
$args = contentDocArgs();

$wp_query = new WP_Query($args);
if( $wp_query->have_posts() ):
	while ( $wp_query->have_posts() ) : $wp_query->the_post();
		get_template_part( 'parts/loop', 'archive-doc' );
	endwhile;
endif;?>
<?php joints_page_navi(); ?>
	</main>
</div>
<?php get_footer('extranet');
function contentDocArgs(){
	global $query;
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$args =  array(
			'paged' => $paged,
			'page' => $paged,
			'post_type' => 'doc',
			'post_status' => 'publish',
		);
	if(isset($_POST['searchFormDoc']))
	{
		if(!empty($_POST['keywords'])){
			$args['s'] = $_POST['keywords'];
		}

		if( !empty($_POST['categorie']) ){
			$args['tax_query'] = array(
					array(	'taxonomy' => 'categorie',
							'field'    => 'term_id',
							'terms'    => $_POST['categorie']
						),
					);
			return $args;
		}
	}

	return $args;
}
