<?php
/*
Template Name: Présentation
*/
?>
<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div id="pre-content">
			<?php get_template_part( 'parts/nbr', 'adherents' ); ?>
			<?php if ( has_post_thumbnail() ) :?>
				   <div id="img-pre-content" style="background-image:url(<?php the_post_thumbnail_url( 'img-pre-content'); ?>)"></div>
		   	<?php endif;?>
		   	<?php if(!empty(get_field('fil_ariane'))):?>
			   <div class="fil_ariane">
				   <p>
					   <?php if(!empty(get_field('picto_ariane'))):?>
						   <?php the_field('picto_ariane');?>
					   <?php endif;?>
					   <?php the_field('fil_ariane');?>
				   </p>
			   </div>
		   <?php endif;?>
		   <main id="content">
			   <?php if(!empty(get_field('chapeau'))):?>
				   <?php the_field('chapeau');?>
				   <p><a href="<?php echo get_option('fivape_url_dest_adhesion');?>" target="_blank" class="button button-radius button-blue button-medium">
					   Rejoindre la FIVAPE
				   </a></p>
			   <?php endif;?>

				<?php if( have_rows('historique') ): ?>
					<div class="accordion">
				    <?php while ( have_rows('historique') ) : the_row(); ?>
				        <div class="accordion-item">
							 <div class="accordion-title">
								 <i class="fa fa-plus-square" aria-hidden="true"></i>
								 <?php the_sub_field('annee'); ?>
							 </div>
							 <div class="accordion-content">
								 <?php the_sub_field('item'); ?>
							 </div>
						</div>
				    <?php endwhile;?>
					 </div>
				<?php endif;?>
			</div>
		</div>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
