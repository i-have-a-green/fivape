jQuery(document).ready(function() {
	$('.form-contact').on("submit", function(ev) {
		ev.preventDefault();
		var form = document.forms.namedItem("form-contact");
		var formData = new FormData(form);
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: formData,
			success: function (data) {
				console.log(data+'yes');
				el = document.getElementById('modalTest');
				text = '<p>Votre message a été envoyé,<br>il sera traité rapidement.</p>';
				openModal(el, text);
			},
			cache: false,
			contentType: false,
			processData: false
		});
	});
	$('.form-formation').on("submit", function(ev) {
		ev.preventDefault();
		var form = document.forms.namedItem("form-formation");
		var formData = new FormData(form); 
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: formData, 
			success: function (data) {
				console.log(data+'yes');
				el = document.getElementById('modalTest');
				text = '<p>Votre message a été envoyé,<br>il sera traité rapidement.</p>';
				openModal(el, text);
			},
			cache: false,
			contentType: false,
			processData: false
		});
	});
	$('.form-adhesion').on("submit", function(ev) {
		ev.preventDefault();
		var form = document.forms.namedItem("form-adhesion");
		var formData = new FormData(form);
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: formData,
			success: function (data) {
				console.log(data+'yes');
				el = document.getElementById('modalTest');
				text = '<p>Votre message a été envoyé,<br>il sera traité rapidement.</p>';
				openModal(el, text);
			},
			cache: false,
			contentType: false,
			processData: false
		});
	});
});
