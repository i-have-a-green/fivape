jQuery(document).ready(function() {
	$('.button-transparent').hover(
		function(){
			if($(this).attr('data-color')){
				$(this).css('color', $(this).attr('data-color'));
			}
			else{
				$(this).css('color', '#6db5db');
			}
		},
		function(){
			$(this).css('color', '#FFF');
		});
});
