jQuery(document).ready(function() {
	if($('#formForum').length){
		$('.tag').on('click', function(ev){
			ev.preventDefault();
			$('.tag').removeClass('active');
			$(this).addClass('active');
			$('#tag').val($(this).attr('data-value'))
			console.log($('#tag').val());
		});
	}

	if($('#formCreateForum').length){
		$('.tag').on('click', function(ev){
			ev.preventDefault();
			if($(this).hasClass('active')){
				$(this).removeClass('active');
			}
			else{
				$(this).addClass('active');
			}

			var v = "";
			$('.tag.active').each(function(){
				v += $(this).attr('data-value') + "-";
			});
			$('#tag').val(v);
			console.log(v);
		});
	}
});

function hasClass(el, className) {
  if (el.classList)
    return el.classList.contains(className)
  else
    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className)
  else if (!hasClass(el, className)) el.className += " " + className
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className)
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
    el.className=el.className.replace(reg, ' ')
  }
}
