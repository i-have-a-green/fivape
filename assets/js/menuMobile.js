function menuMobile() {
	el = document.getElementById("menuMobile");
	overlay = document.getElementById("menuOverlay");
	if(hasClass(el, 'menuMobileOpen')){
		console.log("close menu mobile");
		removeClass(el, 'menuMobileOpen');
		removeClass(overlay, 'overlayOpen');
	}
	else{
		console.log("open menu mobile");
		addClass(el, 'menuMobileOpen');
		addClass(overlay, 'overlayOpen');
	}
}
