jQuery(document).ready(function() {
	if($('.accordion').length){
		$('.accordion-item').on('click', function(){
			if($(this).hasClass('accordion-open')){
				console.log('accordion open');
				$(this).removeClass('accordion-open');
			}
			else{
				console.log('accordion close');
				$(this).addClass('accordion-open');
			}
		});
	}
	$('.BtnModalConnect').on('click', function(){
		openModal(document.getElementById('modalConnect'));
	});

});

function hasClass(el, className) {
  if (el.classList)
    return el.classList.contains(className)
  else
    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className)
  else if (!hasClass(el, className)) el.className += " " + className
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className)
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
    el.className=el.className.replace(reg, ' ')
  }
}


/********MODAL*******/
function openModal(el, text = ""){
	console.log('open modal');
	if(text.length){
		el.children[1].innerHTML = text;
	}
	addClass(el, 'openModal');
	overlay = document.getElementById("modalOverlay");
	addClass(overlay, 'overlayOpen');
}
function closeModal(){
	el = document.getElementsByClassName('modal');
	for(var i = 0; i < el.length; i++){
		removeClass(el[i], 'openModal');
	}
	overlay = document.getElementById("modalOverlay");
	removeClass(overlay, 'overlayOpen');
}
