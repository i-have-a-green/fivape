<?php 
	/* Template Name: Accueil */ 
	get_header(); 
	$postsCarousel = array();
?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main class="home">

			<?php if( have_rows('bloc_carousel') ): ?>
			<section class="carousel" style="background-image:url();">
				<div class="container d-flex align-items-center">
					<div class="row">
						<div class="col-12 col-md-10 col-lg-8 d-flex align-items-center">
							<nav class="bullets">
								<?php while( have_rows('bloc_carousel') ): the_row(); if(get_row_index() == 1){ $navClass = "current"; }else{ $navClass = ""; }; ?>
									<a href="#" class="<?php echo $navClass; ?>"></a>
								<?php endwhile; ?>
								</ul>
							</nav>
							<div class="wrapper">
								<?php 
									while( have_rows('bloc_carousel') ): the_row();
									if(get_row_index() == 1){ $navClass = "active"; }else{ $navClass = ""; };
									$featuredPost = get_sub_field('featured_post');
									$featuredPostID = $featuredPost->ID;
									$postsCarousel[] = $featuredPostID;
									$headerImage = get_field('image',$featuredPostID);
								?>
									<article class="item-news <?php echo $navClass; ?>">
										<img src="<?php echo $headerImage['sizes']['header']; ?>" width="<?php echo $headerImage['sizes']['header-width']; ?>" height="<?php echo $headerImage['sizes']['header-height']; ?>" alt="<?php echo $headerImage['alt']; ?>" title="<?php echo $headerImage['title']; ?>" class="hidden">
										<div class="infos">
											<span class="date"><?php echo get_post_time('d M Y',true,$featuredPostID,true); ?></span> - 
											<span class="categ"><?php the_field('category',$featuredPostID); ?></span>
										</div>
										<h2><?php echo get_the_title($featuredPostID); ?></h2>
										<p><?php echo get_the_excerpt($featuredPostID); ?></p>
										<a href="<?php echo get_permalink($featuredPostID); ?>" class="btn">Lire la suite</a>
									</article>
									<?php endwhile; ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php endif; ?>
			
			<?php 
				$news = new WP_Query(
					array(
						'no_found_rows' => true,
						'posts_per_page' => 4, 
						'order' => 'DESC',
						'post__not_in' => $postsCarousel
					)
				);
				if ( $news->have_posts() ) : 
			?>
			<section class="news">
				<div class="container">
					<div class="row">
						<?php while ( $news->have_posts() ) : $news->the_post(); 
							$categories = get_the_category( $post->ID );
							$categories_string = join(', ', wp_list_pluck($categories, 'name')); 
						?>
						<article class="col-12 col-md-6 col-lg-3 item-news">
							<a href="<?php the_permalink(); ?>">
								<div class="inner">
									<img src="<?php echo get_field('image_home')['sizes']['news']; ?>" width="<?php echo get_field('image_home')['sizes']['news-width']; ?>" height="<?php echo get_field('image_home')['sizes']['news-height']; ?>" alt="<?php echo get_field('image_home')['alt']; ?>" title="<?php echo get_field('image_home')['title']; ?>">
									<span class="date"><?php echo get_post_time('d M Y',true,get_the_ID(),true); ?></span>
								</div>
								<h2><?php the_title(); ?></h2>
								<span class="categ"><?php echo $categories_string; ?></span>
								<p><?php the_excerpt(); ?></p>
								<span class="more">→ Lire la suite</span>
							</a>
						</article>
						<?php endwhile; ?>
					</div>
					<div class="row justify-content-center">
						<div class="col-auto">
							<a href="https://www.fivape.org/actualite/" class="btn">Voir plus d'actualités</a>
						</div>
					</div>
				</div>
			</section>
			<?php endif; wp_reset_postdata(); ?>
			
			<?php if( have_rows('about') ): while( have_rows('about') ): the_row(); ?>
			<?php if( get_sub_field('intro')['content'] ): ?>
			<section class="about">
				<div class="container">
					<div class="row text justify-content-center">
						<div class="col-10">
							<h1><?php the_sub_field('intro')['title']; ?></h1>
							<?php the_sub_field('intro')['content']; ?>
						</div>
					</div>
					<?php if( have_rows('list') ): ?>
					<div class="row features justify-content-center">
						<?php 
							$countAboutList = count(get_sub_field('list'));
							if( $countAboutList == 2 ){
								$aboutListClass = 'col-12 col-sm-6 col-md-5';
							}elseif( $countAboutList == 3 ){
								$aboutListClass = 'col-12 col-sm-6 col-md-4';
							}elseif( $countAboutList == 4 ){
								$aboutListClass = 'col-12 col-sm-6 col-md-3';
							}
							while( have_rows('list') ): the_row(); 
								if($countAboutList == 2 && get_row_index() == 2){
									$offsetList = 'offset-none offset-md-1';
								}else{
									$offsetList = '';
								}
						?>
						<div class="<?php echo $aboutListClass.' '.$offsetList; ?> item">
							<img src="<?php echo get_sub_field('image')['sizes']['medium']; ?>" width="<?php echo get_sub_field('image')['sizes']['medium-width']; ?>" height="<?php echo get_sub_field('image')['sizes']['medium-height']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>" title="<?php echo get_sub_field('image')['title']; ?>">
							<h2><?php the_sub_field('title'); ?></h2>
							<p><?php the_sub_field('text'); ?></p>
						</div>
						<?php endwhile; ?>
					</div>
				</div>
			</section>
			<?php endif; ?>
			<?php endwhile; endif; ?>
			
			<?php $hero = get_field('hero'); if( $hero['title'] ): ?>
			<section class="hero" style="background-image:url(<?php echo $hero['image']['sizes']['header']; ?>)">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-11 col-md-9 col-lg-6">
							<h2><?php echo $hero['title']; ?></h2>
							<p><?php echo $hero['text']; ?></p>
							<a href="<?php echo $hero['btn']['link']; ?>" class="btn"><?php echo $hero['btn']['label']; ?></a>
						</div>
					</div>
				</div>
			</section>
			<?php endif; ?>
			
			<?php if( have_rows('classic') ): while( have_rows('classic') ): the_row(); 
				$classic_col_nb = get_sub_field('classic_col_nb');
				if ($classic_col_nb == 1){ 
					$classic_col_class = 'col-12 col-lg-10'; 
				}
				elseif ($classic_col_nb == 2){ 
					$classic_col_class = 'col-12 col-lg-5'; 
				}
			?>
			<?php if( have_rows('list') ): ?>
			<section class="classic">
				<div class="container">
					<div class="row justify-content-center align-items-center">
						<?php while( have_rows('list') ): the_row(); ?>
							<div class="<?php echo $classic_col_class; ?> base">
								<?php the_sub_field('content'); ?>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</section>
			<?php endif; ?>
			<?php endwhile; endif; ?>

		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>
