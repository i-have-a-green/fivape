<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div id="pre-content">
			<?php if ( has_post_thumbnail() ) :?>
					<div id="img-pre-content" style="background-image:url(<?php the_post_thumbnail_url( 'img-pre-content'); ?>)"></div>
			<?php endif;?>
				<div class="fil_ariane" style="<?php if(!empty(get_field('bg'))){echo 'background-color:'.get_field('bg');}?>">
					<h1><p><?php the_title();?></p></h1>
				</div>
			<main id="content">
				<?php the_content();?>
				<div class="author">
					<strong><?php the_author() ?></strong>, le <?php the_date();?>
				</div>
				<nav>
					<?php previous_post_link( '%link', "Article précédent" ); ?><?php next_post_link( '%link', "Article suivant" ); ?>
				</nav>
			</main>
		</div>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
