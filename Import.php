<?php 
/*
Template Name: Import
 */
set_time_limit(0);
if (!function_exists('media_handle_upload')) {
  require_once(ABSPATH . 'wp-admin/includes/image.php');
  require_once(ABSPATH . 'wp-admin/includes/file.php');
  require_once(ABSPATH . 'wp-admin/includes/media.php');
}
$dir = wp_upload_dir();

$actionInsert = true;
$actionRead = true;
$actionLimit = 99910;

function get_attachment_id($url)
{

  $attachment_id = 0;
  $dir = wp_upload_dir();
  $url = str_replace("http://fivape.romainpetiot.com/", "https://www.fivape.org/", $url);
  if (false !== strpos($url, "https://www.fivape.org/")) { // Is URL in uploads directory?
    $file = basename($url);
    $query_args = array(
      'post_type' => 'attachment',
      'post_status' => 'inherit',
      'fields' => 'ids',
      'meta_query' => array(
        array(
          'value' => $file,
          'compare' => 'LIKE',
          'key' => '_wp_attachment_metadata',
        ),
      )
    );

    $query = new WP_Query($query_args);

    if ($query->have_posts()) {

      foreach ($query->posts as $post_id) {

        $meta = wp_get_attachment_metadata($post_id);

        $original_file = basename($meta['file']);
        $cropped_image_files = wp_list_pluck($meta['sizes'], 'file');

        if ($original_file === $file || in_array($file, $cropped_image_files)) {
          $attachment_id = $post_id;
          break;
        }

      }

    }

  }
  if ($attachment_id == 0) {
    echo 'problème image : ' . $url . '<br>';
  }
  return $attachment_id;
}

get_header(); ?>
  <?php $p = get_page_by_path("media"); ?>
	<div id="pre-content">
		<?php get_template_part('parts/nbr', 'adherents'); ?>
		<?php if (!empty(get_field('fil_ariane_', $p))) : ?>
			<div class="fil_ariane">
				<p>
					<?php if (!empty(get_field('picto_ariane', $p))) : ?>
						<?php the_field('picto_ariane', $p); ?>
					<?php endif; ?>
					<?php the_field('fil_ariane_', $p); ?>
				</p>
			</div>
		<?php endif; ?>
		<main id="content">
      <div>

<?php

if (have_rows('paragraphes', 58)) : $i = 0; ?>
<?php while (have_rows('paragraphes', 58)) : the_row();
$titre = get_sub_field('titre');
$content = get_sub_field('contenu');

if (!empty($titre)) {

  if (substr($titre, -4, 4) == "2018") {

    $postdate = '2018-' . substr($titre, -7, 2) . '-' . substr($titre, -10, 2) . ' 00:00:00';
    $titre = substr($titre, 0, -10);
    if (substr($titre, -3, 3) == " - ") {
      $titre = substr($titre, 0, -3);
    }
  } else if (substr($titre, -3, 3) == "/18") {
    $postdate = '2018-' . substr($titre, -5, 2) . '-' . substr($titre, -8, 2) . ' 00:00:00';
    $titre = substr($titre, 0, -10);
    if (substr($titre, -3, 3) == " - ") {
      $titre = substr($titre, 0, -3);
    }
  }


  $href = "";
  $src = "";
  $description = "";



  $dom = new DOMDocument;
  @$dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
  $links = $dom->getElementsByTagName('a');
  foreach ($links as $link) {
    $href = $link->getAttribute('href');
    break;
  }

  $imgs = $dom->getElementsByTagName('img');
  foreach ($imgs as $img) {
    $src = $img->getAttribute('src');
    break;
  }
  if ($actionRead) {
    $html = @file_get_contents($href);
    if ($html !== false) {
      $dom = new DOMDocument;
      @$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));


      $finder = new DomXPath($dom);
      $classname = "lead";
      foreach ($finder->query("//*[contains(@class, '$classname')]") as $lead) {
        $description = $lead->nodeValue;
      }
      if (empty($description)) {
        $metas = $dom->getElementsByTagName('meta');
        foreach ($metas as $meta) {
          if ($meta->getAttribute('name') == "twitter:description" || $meta->getAttribute('property') == "twitter:description" || $meta->getAttribute('property') == "og:description") {
            if (!empty($meta->getAttribute('content'))) {
              $description = $meta->getAttribute('content');
            }
          } else if ($meta->getAttribute('name') == "description") {
            if (!empty($description) && !empty($meta->getAttribute('content'))) {
              $description = $meta->getAttribute('content');
            }
          }
        }
      }
    } else {
      echo '<span style="color:red;">problème load : ' . $href . '</span><br>';
    }
  }

  $post = [];
  $post['post_type'] = 'press';
  $post['post_status'] = 'publish';
  $post['post_content'] = false;
  $post['post_title'] = $titre;
  $post['post_date'] = $postdate;
  $post['post_excerpt'] = $description;

  $attachment_id = get_attachment_id($src);

  if ($actionInsert) {

    $p = wp_insert_post($post, true);
    if (is_wp_error($p)) {
      echo "problème : " . $titre . "<br>";
    } else {
      update_field('lien', $href, $p);
      if ($attachment_id > 0) {
        set_post_thumbnail($p, $attachment_id);
      }
    }
  }

}
$i++;
if ($i > $actionLimit) {
  break;
}

endwhile; ?>
<?php endif; ?>



      
    </main>
  </div>
<?php get_footer(); ?>
