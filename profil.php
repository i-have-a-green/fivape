<?php
/*
Template Name: Profil
*/

if(isset($_POST['action']) && $_POST['action'] == 'edit-customuser'){
	rp_fivape_update_user();
}

?>
<?php get_header('extranet'); ?>
<div id="pre-content">
	<div class="fil_ariane">
		<p>
			<?php the_title();?>
		</p>
	</div>
	<main id="content">
<?php echo rp_fivape_form(true);?>
	</main>
</div>
<?php get_footer('extranet'); ?>
