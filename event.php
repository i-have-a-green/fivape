<?php
/*
Template Name: Event
*/
get_header('extranet'); ?>
<div id="pre-content">
	<div class="fil_ariane event">
		<p>
			<?php the_title();?>
		</p>
	</div>
	<main id="content">
		<form method="post" action="" id="formEvent" class="formEvent">
			<div>
				<label><b>Type</b></label>
				<select name="type">
					<option value="">
						Tous
					</option>
					<?php
					$terms = get_terms('type', array('hide_empty' => true));

					foreach($terms as $term){
						$checked = false;
						if(isset($_POST['type']) && $_POST['type'] == $term->term_id){
							$checked = true;
						}
						echo '<option value="'.$term->term_id.'" '.selected( $checked, true, false ).'>
						'.$term->name.'
						</option>';
					}
					?>
				</select>
			</div>
			<div>
				<label><b>Recherche</b></label>
				<input type="text" placeholder="Entrer votre recherche" name="keywords" />
			</div>
			<div id="cont-button">
				<button type="submit" name="searchForm" class="button">Rechercher</button>
			</div>
		</form>
<?php
$args = contentInspirationArgs();

$wp_query = new WP_Query($args);
if( $wp_query->have_posts() ):
	while ( $wp_query->have_posts() ) : $wp_query->the_post();
		get_template_part( 'parts/loop', 'archive-event' );
	endwhile;
endif;?>
<?php joints_page_navi(); ?>
	</main>
</div>
<?php get_footer('extranet');
function contentInspirationArgs(){
	global $query;
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$args =  array(
			'paged' => $paged,
			'page' => $paged,
			'post_type' => 'event',
			'post_status' => 'publish',
			'orderby' => 'meta_value_num',
			'order'	=> 'DESC',
			'meta_type' => 'DATE',
			'meta_key' => 'date'
		);
	if(isset($_POST['searchForm']))
	{
		if(!empty($_POST['keywords'])){
			$args['s'] = $_POST['keywords'];
		}

		if( !empty($_POST['type']) ){
			$args['tax_query'] = array(
					array(	'taxonomy' => 'type',
							'field'    => 'term_id',
							'terms'    => $_POST['type']
						),
					);
			return $args;
		}
	}

	return $args;
}
