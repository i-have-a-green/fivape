<?php
/*
Template Name: CreateForum
*/
if(isset($_POST['createForm'])){
	$post = [];
	$post['post_type']   = 'forum';
	$post['post_status'] = 'publish';
	$post['post_title'] = sanitize_text_field($_POST['titre']);
	$post['post_content'] = $_POST['post_content'];

	$id = wp_insert_post( $post, true );
	wp_set_post_terms( $id, array($_POST['repertoire']), "repertoire");

	if(!empty($_POST['tag'])){
		$tag = explode('-', substr( $_POST['tag'],0,-1) );
		foreach ($tag as $key => $value) {
			$tag[$key] = (int) $tag[$key];
		}
		wp_set_post_terms( $id, $tag, "tag");
	}
	wp_redirect(get_the_permalink( $id ), 302);
	exit();
}


get_header('extranet'); ?>
<div id="pre-content">
	<div class="fil_ariane forum">
		<p>
			<?php the_title();?>
		</p>
	</div>
	<main id="content">
		<form method="post" action="" id="formCreateForum">
			<div>
				<b>Titre du sujet</b><br />
				<input type="text" placeholder="Titre du sujet" name="titre" required />
			</div>
			<div>
				<b>Commentaires</b><br />
				<!--<textarea placeholder="Création de post" name="post_content" /></textarea>-->
				<?php $settings = array(
          'media_buttons' => false,
			        'wpautop' => true,

'quicktags' => false
					    /*
			        /*'tinymce' => array(
			            'theme_advanced_buttons1' => 'formatselect,|,bold,italic,underline,|,' .
			                'bullist,blockquote,|,justifyleft,justifycenter' .
			                ',justifyright,justifyfull,|,link,unlink,|' .
			                ',spellchecker,wp_fullscreen,wp_adv ',

			        ),*/
			        //
			    );
			    wp_editor('', 'post_content', $settings ); ?>
			</div>
			<div>
				<b>Répertoire</b>
				<select name="repertoire">
					<?php
					$terms = get_terms('repertoire', array('hide_empty' => true));
					foreach($terms as $term){
						echo '<option value="'.$term->term_id.'" '.selected( $checked, true, false ).'>
						'.$term->name.'
						</option>';
					}
					?>
				</select>
			</div>
			<div>
				<b>Tags</b><br />
				<input type="hidden" value="<?php if(isset($_POST['tag']) ){echo $_POST['tag'];}?>" name="tag" id="tag">
				<?php
				$terms = get_terms('tag', array('hide_empty' => true));
					foreach($terms as $term){
						$checked = '';
						if(isset($_POST['tag']) && $_POST['tag'] == $term->term_id){
							$checked = 'active';
						}
						echo '<button data-value="'.$term->term_id.'" class="button button-radius tag '.$checked.'">
						'.$term->name.'
						</button>';
					}
					?>
				</select>
			</div>
			<div>
				<button type="submit" name="createForm" class="button button-forum">Création du sujet</button>
			</div>
		</form>
	</main>
</div>
<?php get_footer('extranet'); ?>
