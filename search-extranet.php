<?php
/*
Template Name: Extranet-search
*/
?>
<?php get_header('extranet'); ?>
<div id="pre-content">
	<div class="fil_ariane">
		<p>
			Recherche
		</p>
	</div>
	<main id="content" class=" extranet">
		<?php global $query;
		$args =  array(
				'post_type' => array('forum', 'event', 'sondage', 'doc'),
				'post_status' => 'publish',
			);
		if(isset($_POST['search'])){
			$args['s'] = $_POST['search'];
		}
		$wp_query = new WP_Query($args);
		if( $wp_query->have_posts() ):
			while ( $wp_query->have_posts() ) : $wp_query->the_post();
				get_template_part( 'parts/loop', 'archive-'.$post->post_type );
			endwhile;
		endif;
		?>
	</main>
</div>
<?php get_footer('extranet');
