<?php get_header('extranet'); ?>
	<div id="pre-content">
		<div class="fil_ariane sondage">
			<p>
				Sondage
			</p>
		</div>
		<main id="content">
			<div id="pre-content">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<p>
					<h1><?php the_title();?></h1>
				</p>
				<p>
					<?php the_field('description');?>
				</p>
				<?php the_content(); ?>
		<?php endwhile; endif; ?>
			</div>
		</main>
	</div>
<?php get_footer(); ?>
