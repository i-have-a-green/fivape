<?php
/*
Template Name: Extranet
*/
?>
<?php get_header('extranet'); ?>
<div id="pre-content" class="extranet">
	<main id="">
		<?php
			Args('forum', 5, 'Derniers messages du Forum', 'extranet/forum/');
			Args('doc', 3, 'Derniers Documents ajoutés', 'extranet/documents/');
			Args('sondage', 7, 'Derniers sondages', 'extranet/sondage/');
			Args('event', 4, 'Calendrier des évenements', 'extranet/evenements/');
		?>
	</main>
</div>
<?php get_footer('extranet');
function Args($cpt, $nbr, $txt, $link, $option=""){
	?>
	<div class="extranet-<?php echo $cpt;?>">
		<div class="fil_ariane <?php echo $cpt;?>">
			<p>
				<a href="<?php echo home_url($link);?>"><?php echo $txt;?></a>
			</p>
		</div>
	<?php
	global $query;
	if($cpt != "event"){
		$args =  array(
			'posts_per_page' => $nbr,
			'post_type' => $cpt,
			'post_status' => 'publish',
		);
	}
	else{
		$args =  array(
			'posts_per_page' => $nbr,
			'post_type' => $cpt,
			'post_status' => 'publish',
			'orderby' => 'meta_value_num',
			'order'	=> 'DESC',
			'meta_type' => 'DATE',
			'meta_key' => 'date'
		);
	}
	
	$wp_query = new WP_Query($args);
	if( $wp_query->have_posts() ):
		while ( $wp_query->have_posts() ) : $wp_query->the_post();
			get_template_part( 'parts/loop', 'archive-'.$cpt );
		endwhile;
	endif;
	echo '</div>';
}
?>
