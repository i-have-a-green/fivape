<?php
/*
Template Name: Gutenberg
*/
?>
<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div id="pre-content">
			<?php get_template_part( 'parts/nbr', 'adherents' ); ?>
			<?php if ( has_post_thumbnail() ) :?>
					<div id="img-pre-content" style="background-image:url(<?php the_post_thumbnail_url( 'img-pre-content'); ?>)"></div>
			<?php endif;?>
			<?php if(!empty(get_field('fil_ariane_'))):?>
				<div class="fil_ariane">
					<p>
						<?php if(!empty(get_field('picto_ariane'))):?>
							<?php the_field('picto_ariane');?>
						<?php endif;?>
						<?php the_field('fil_ariane_');?>
					</p>
				</div>

			<?php endif;?>
			<main id="content">
				<h1><?php the_title();?></h1>
				<?php the_content();?>
			</main>
		</div>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>
