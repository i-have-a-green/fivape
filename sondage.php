<?php
/*
Template Name: Sondage
*/
get_header('extranet'); ?>
<div id="pre-content">
	<div class="fil_ariane sondage">
		<p>
			<?php the_title();?>
		</p>
	</div>
	<main id="">
		<form method="post" action="" id="formSondage" class="formSondage">
			<div>
				<label><b>Recherche</b></label>
				<input type="text" placeholder="Entrer votre recherche" name="keywords" />
			</div>
			<div id="cont-button">
				<button type="submit" name="searchForm" class="button">Rechercher</button>
			</div>
		</form>
<?php
$args = contentInspirationArgs();

$wp_query = new WP_Query($args);
if( $wp_query->have_posts() ):
	while ( $wp_query->have_posts() ) : $wp_query->the_post();
		get_template_part( 'parts/loop', 'archive-sondage');
	endwhile;
endif;?>
<?php joints_page_navi(); ?>
	</main>
</div>
<?php get_footer('extranet');
function contentInspirationArgs(){
	global $query;
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$args =  array(
			'paged' => $paged,
			'page' => $paged,
			'post_type' => 'sondage',
			'post_status' => 'publish',
		);
	if(isset($_POST['searchForm']))
	{
		if(!empty($_POST['keywords'])){
			$args['s'] = $_POST['keywords'];
		}
	}
	return $args;
}
